<?php
class DB{
	private $_mysqli,
 $_query,
 $_results = array(),
 $_count = 0;

 public static $instance;

 public static function getInstance()
 {
  if( !isset( self :: $instance ) )
  {
     self :: $instance = new DB();
 }
 return self :: $instance;
}			

public function __construct()
{
		//$this -> _mysqli = new mysqli('localhost', 'nshcomua', 'HG6fmcjc0ybb16g', 'nshcomua_cn');
    $this -> _mysqli = new mysqli('localhost', 'noroxcom_berna', 'U5T8h5SgjPsI', 'noroxcom_berna');
  $this -> _mysqli -> query("SET NAMES 'utf8';");
  $this -> _mysqli -> query("SET CHARACTER SET 'utf8';");
  $this -> _mysqli -> query("SET SESSION collation_connection = 'utf8_general_ci';");
  if( $this -> _mysqli -> connect_error )
  {
     die( $this -> mysqli -> mysqli_connect_error );
 }
}

public function query($sql)
{
  if( $this -> _query = $this -> _mysqli -> query($sql) )
  {
    if( $this -> _query -> num_rows > 0)
    {
     while( $row = $this -> _query -> fetch_object() )
     {
        $this -> _results[] = $row;
     }
     $this -> _count = $this -> _query -> num_rows;
    }
    else
    {
      $this -> _count = 0;
    }
}
return $this;
}

public function no_returns($sql)
{
  $this -> _query = $this -> _mysqli -> query($sql);
}

public function results()
{
  return $this -> _results;
}

public function count()
{
  return $this -> _count;
}
//----------------Authorization and registration
public function storeUser($name, $lastname, $email, $password) {
    $uuid = uniqid('', true);
    $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt16
        $empty = '';
        $status = 'active';
        $priv = 'user';
        $is_online = 1;

        $stmt = $this -> _mysqli -> query("INSERT INTO us_main(unique_id, sys_email, sys_telephone, encrypted_password,sys_link,sys_avatar,sys_status,sys_priv,main_name,main_lastname,main_status, salt) VALUES(
             '".$uuid."', '".$email."', '".$empty."', '".$encrypted_password."', '".$empty."','".$empty."','".$status."','".$priv."', '".$name."', '".$lastname."', '".$empty."', '".$salt."')");

        //$stmt = $this-> _mysqli ->prepare();
        //$stmt->bind_param("ssssssssssssss", $uuid, $email, $empty, $encrypted_password, $empty,$empty,$status,$priv, $name, $lastname, $empty, $salt);
        //$result = $stmt->execute();
        //$stmt->close();

        // check for successful store
        if ($stmt)
        {
            $stmt = $this -> _mysqli -> query("SELECT * FROM us_main WHERE sys_email = '$email'");
            $user = $stmt -> fetch_assoc();

            return $user;
        } else {
            return false;
        }
    }

    /**
     * Get user by email and password
     */
    public function getUserByEmailAndPassword($email, $password) {

        $stmt = $this-> _mysqli ->query("SELECT * FROM us_main WHERE sys_email = '$email'");

        $user = $stmt->fetch_assoc();

            // verifying user password
        $salt = $user['salt'];
        $encrypted_password = $user['encrypted_password'];
        $hash = $this->checkhashSSHA($salt, $password);
            // check for password equality
        if ($encrypted_password == $hash) {
                // user authentication details are correct
            return $user;
        }
    }

    /**
     * Check user is existed or not
     */
    public function isUserExisted($email) {
        $stmt = $this-> _mysqli ->prepare("SELECT sys_email from us_main WHERE sys_email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */
    public function hashSSHA($password) {

        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

    /**
     * Decrypting password
     * @param salt, password
     * returns hash string
     */
    public function checkhashSSHA($salt, $password) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }

}
?>