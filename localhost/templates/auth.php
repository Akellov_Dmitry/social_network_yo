<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cool net</title>
	<link rel="stylesheet" href="/css/auth.css" />
</head>
<body>
	<img id="header_img" src="./img/auth_header.png" alt="" />
	<div id="auth_cont">
		<label id="head">Authorization</label>
		<div><label>Email</label><input id="auth_login" type="email" /><div class="border_add1"></div></div>
		<div><label>Password</label><input id="auth_pass" type="password" /><div class="border_add2"></div></div>
		<button id="go_but">Go</button>
		<button id="to_reg_but">Registration</button>
	</div>
	<div id="reg_cont">
		<label id="head">Registration</label>
		<div><label>Name and surname</label><input id="reg_name" type="text" /><div class="border_reg_add1"></div></div>
		<div><label>Email</label><input id="reg_email" type="email" /><div class="border_reg_add2"></div></div>
		<div><label>Password</label><input id="reg_pass" type="password" /><div class="border_reg_add3"></div></div>
		<div><label>Repeat password</label><input id="reg_pass_rep" type="password" /><div class="border_reg_add4"></div></div>
		<button id="reg_but">Registration</button>
		<button id="to_auth_but">Authorization</button>
	</div>
 	</div>
 <div id="content_view">
	<?php include 'views/'.$content_view; ?>
 </div>	
	<script src="/libs/jquery.js"></script>
	<script src="/js/auth.js"></script>
</body>
</html>