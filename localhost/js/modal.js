/*function getSizes(im,obj)
{
		var x_axis = obj.x1;
		var x2_axis = obj.x2;
		var y_axis = obj.y1;
		var y2_axis = obj.y2;
		var thumb_width = obj.width;
		var thumb_height = obj.height;
		if(thumb_width > 0)
			{
				if(confirm("Do you want to save image..!"))
					{
						$.ajax({
							type:"GET",
							url:"/progs/upload_ava.php?t=ajax&img="+$("#image_name").val()+"&w="+thumb_width+"&h="+thumb_height+"&x1="+x_axis+"&y1="+y_axis,
							cache:false,
							success:function(rsponse)
								{
								 $("#cropimage").hide();
								    $("#thumbs").html("");
									$("#thumbs").html("<img src='users/photo/"+rsponse+"' />");
								}
						});
					}
			}
		else
			alert("Please select portion..!");
	}*/
$(document).ready(function() {
	$("#aded_action").click(function(){
		$("#overlay").css("display","block");
		$("#aded_modal").css("display","block");
	});

	$("#main_menu_icons > i:nth-child(1)").click(function(){
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		//showMyFace();
	});

	$("#overlay").click(function(){
		$("#overlay").css("display","none");
		$(".modal_window").css("display","none");
		$(".photo_container").next().css("display","none");
		$("#video_call_modal").css("display","none");
		//ex();
	});

	$(".modal_window_close").click(function(){
		$("#overlay").css("display","none");
		$(".modal_window").css("display","none");
	});

	$(".modal_cideo_call_modal_close").click(function(){
		$("#overlay").css("display","none");
		$(".modal_window").css("display","none");
		//ex();
	});

	$(".end_call_button").click(function(){
		$(".modal_window").css("display","none");
		$("#overlay").css("display","none");
		//ex();
	});

	$("#aded_modal_photo_but").click(function(){
		$("#aded_modal_photo").css("display","block");
		$("#aded_modal_video").css("display","none");
		$("#aded_modal_audio").css("display","none");
	});
	$("#aded_modal_video_but").click(function(){
		$("#aded_modal_photo").css("display","none");
		$("#aded_modal_video").css("display","block");
		$("#aded_modal_audio").css("display","none");
	});
	$("#aded_modal_audio_but").click(function(){
		$("#aded_modal_photo").css("display","none");
		$("#aded_modal_video").css("display","none");
		$("#aded_modal_audio").css("display","block");
	});

	$(".photo_container").click(function(){
		$("#overlay").css("display","block");
		$(this).next().css("display","block");
	});

	$("#main_settings_but").click(function(){
		$("#main_settings").css("display","block");
		$("#notice_settings").css("display","none");
		$("#develop_settings").css("display","none");
	});

	$("#notice_settings_but").click(function(){
		$("#main_settings").css("display","none");
		$("#notice_settings").css("display","block");
		$("#develop_settings").css("display","none");
	});

	$("#develop_settings_but").click(function(){
		$("#main_settings").css("display","none");
		$("#notice_settings").css("display","none");
		$("#develop_settings").css("display","block");
	});

	$("#develop_settings_start_but").click(function(){
		$("#develop_settings_start").css("display","block");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_auth_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","block");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_info_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","block");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_notice_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","block");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_friends_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","block");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_music_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","block");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_gallery_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","block");
	});
});