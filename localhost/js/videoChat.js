var config = 
{
    apiKey: "AIzaSyA7m139DR91sbHinG8_QzVE8WgO-yAD7Qc",
    authDomain: "snnsh-5e384.firebaseapp.com",
    databaseURL: "https://snnsh-5e384.firebaseio.com",
    projectId: "snnsh-5e384",
    storageBucket: "",
    messagingSenderId: "493174809361"
};
firebase.initializeApp(config);

var yourVideo = document.getElementById("yourVideo");
var friendsVideo = document.getElementById("friendsVideo");
var myUsername;
var targetUsername;
var servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
var pc;
var database = firebase.database().ref();
var userTarget;

//function connect() 
//{
    database = firebase.database().ref();
    yourVideo = document.getElementById("yourVideo");
    friendsVideo = document.getElementById("friendsVideo");
    myUsername = $("#user_id").val();
    userTarget = targetUsername;
    //Create an account on Viagenie (http://numb.viagenie.ca/), and replace {'urls': 'turn:numb.viagenie.ca','credential': 'websitebeaver','username': 'websitebeaver@email.com'} with the information from your account
    servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
    pc = new RTCPeerConnection(servers);
    pc.onicecandidate = (event => event.candidate?sendToServer({type: "new-ice-candidate", target: userTarget, ice: event.candidate}):console.log("Sent All Ice") );
    pc.onaddstream = (event => friendsVideo.srcObject = event.stream);
	
	database.on('child_added', readMessage);
    
    function readMessage(evt) {
        var jssPARSE = JSON.parse(evt.A.B);
        console.dir(jssPARSE);
        //var type = jssPARSE.type;
        //var sender = jssPARSE.name;
        //var target = jssPARSE.target;
        //var ice = jssPARSE.ice;
        //var sdp = jssPARSE.sdp;
        console.log(jssPARSE.type);
        console.log(jssPARSE.name);
        console.log(jssPARSE.target);
        //console.log(jssPARSE.ice);
        //console.log(jssPARSE.sdp);
        if (jssPARSE.target == myUsername && jssPARSE.type == "call") 
        {
            //alert("Vam zvonit " + jssPARSE.name);
            //console.log("Vam zvonit " + jssPARSE.name);
            document.getElementById(sounds.rington).play();
            alertMsg(jssPARSE);
        }
        if (jssPARSE.name != myUsername) {
            if (jssPARSE.ice != undefined){
                pc.addIceCandidate(new RTCIceCandidate(jssPARSE.ice));
            }
            else if (jssPARSE.type == "video-offer"){
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp))
                  .then(() => pc.createAnswer())
                  .then(answer => pc.setLocalDescription(answer))
                  .then(() => sendToServer({name: myUsername, type: "video-answer", target: userTarget, sdp: pc.localDescription}));
            }
            else if (jssPARSE.type == "video-answer"){
                showFriendsFace();
                document.getElementById(sounds.calling).pause();
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp));
            }
            else if (jssPARSE.type == "hang-up"){
            	closeVideoCall();
            }
            else if (jssPARSE.type == "call-To-Off"){
            	alert(jssPARSE.name + " отклонил ваш вызов");
            }
            else if (jssPARSE.type == "callon"){
            	showFriendsFace();
            }
   		}
};
//}


function alertMsg(jssPARSE) 
{
	var answer = confirm("Вам звонит " + jssPARSE.name + " Желаете ответить на звонок?");
  if (answer) 
	{
    document.getElementById(sounds.rington).pause();
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		showFace();
	}
	else
	{
    document.getElementById(sounds.rington).pause();
    document.getElementById(sounds.end).play();
	sendToServer({name: myUsername, type: "call-To-Off", target: userTarget});
	}
}


function sendToServer(massage) 
{
    var massageJSON = JSON.stringify(massage);
    console.log("Sending '" + massage.type + "' message: " + massageJSON);
    var msg = database.push(massageJSON);
    msg.remove();
}

var sounds = {
            'calling': 'callingSignal',
            'end': 'endCallSignal',
            'rington': 'ringtoneSignal'
        };

function showFace() {
	userTarget = targetUsername;
	sendToServer({type: "callon"});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}
function showMyFace() {
	userTarget = targetUsername;
  	showFriendsFace();
  	document.getElementById(sounds.calling).play();
	sendToServer({name: myUsername, type: "call", target: userTarget});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}

function showFriendsFace() {
  pc.createOffer()
    .then(offer => pc.setLocalDescription(offer) )
    .then(() => sendToServer({name: myUsername, type: "video-offer", target: userTarget, sdp: pc.localDescription}));
}

function hangUpCall() 
{
  closeVideoCall();
  sendToServer({
    name: myUsername,
    type: "hang-up",
    target: userTarget
  });
}

function muteVideo() {

}

function muteAudio() {

}

function closeVideoCall() 
{
  var remoteVideo = document.getElementById("friendsVideo");
  var localVideo = document.getElementById("yourVideo");

  console.log("Closing the call");

  if (pc) 
  {
  	document.getElementById(sounds.calling).pause();
  	document.getElementById(sounds.end).play();
  	console.log("--> Closing the peer connection");

    pc.onaddstream = null;
    pc.ontrack = null;
    pc.onremovestream = null;
    pc.onnicecandidate = null;
    pc.oniceconnectionstatechange = null;
    pc.onsignalingstatechange = null;
    pc.onicegatheringstatechange = null;
    pc.onnotificationneeded = null;

    if (remoteVideo.srcObject) 
    {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    if (localVideo.srcObject) 
    {
      localVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    remoteVideo.src = null;
    localVideo.src = null;

    pc.close();
    pc = null;

    $(".modal_window").css("display","none");
    $("#overlay").css("display","none");

    document.getElementById(sounds.calling).load();
  }
}