$(document).ready(function(){
	$(".admin_act").click(function(){
		var act = $(this).attr("act");
		var id = $(this).next().val();
		$.ajax({
				type: "POST",
				url: "../../progs/admin_act.php",
				data: "act="+act+"&id="+id,
				dataType: "html",
				cache: false,
				async: false,
				success: function(data){
					window.location.reload();
				},
				error: function(data){
					alert(data);
				}
		});
	});
});