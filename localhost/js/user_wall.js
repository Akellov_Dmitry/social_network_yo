var full_href = window.location.href;
	var exp_href = full_href.split('/');
	var href = exp_href[3];


$(document).ready(function() {
		/*----------------------------------------------------------------Implement of colorpicker*/
	if(href == '')
	{
		$('#modal_add_wall_settings_color_bl > div').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$('#modal_add_wall_settings_color_bl > div').css('background-color', '#' + hex);
				$('#modal_add_wall_preview_main').css('background-color', '#' + hex);
				$("#modal_add_wall_preview_back_type").val("col");
				$("#modal_add_wall_preview_back_color1").val($('#modal_add_wall_preview_main').css('background-color'));
				$("#modal_add_wall_preview_back_color2").val('');
			}
		});

		$('#modal_add_wall_settings_colortr_bl > div:nth-child(2)').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				var color1 = '#' + hex;
				var color2 = $('#modal_add_wall_settings_colortr_bl > div:nth-child(3)').css('background-color');

				$("#modal_add_wall_settings_colortr_bl > div:nth-child(2)").css("background",color1);
				$("#modal_add_wall_preview_main").css("background","linear-gradient(280deg, "+color1+", "+ color2+ ")");

				$("#modal_add_wall_preview_back_type").val("grad");
				$("#modal_add_wall_preview_back_color1").val($("#modal_add_wall_settings_colortr_bl > div:nth-child(2)").css("background"));
				$("#modal_add_wall_preview_back_color2").val(color2);
			}
		});

		$('#modal_add_wall_settings_colortr_bl > div:nth-child(3)').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				var color1 = '#' + hex;
				var color2 = $('#modal_add_wall_settings_colortr_bl > div:nth-child(2)').css('background-color');

				$("#modal_add_wall_settings_colortr_bl > div:nth-child(3)").css("background",color1);
				$("#modal_add_wall_preview_main").css("background","linear-gradient(280deg, "+color1+", "+ color2+ ")");

				$("#modal_add_wall_preview_back_type").val("grad");
				$("#modal_add_wall_preview_back_color2").val($("#modal_add_wall_settings_colortr_bl > div:nth-child(3)").css("background"));
				$("#modal_add_wall_preview_back_color1").val(color2);
			}
		});

		$('#modal_add_wall_settings_anim_bl > div:nth-child(2)').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				var color1 = '#' + hex;
				var color2 = $('#modal_add_wall_settings_anim_bl > div:nth-child(3)').css('background-color');

				$("#modal_add_wall_settings_anim_bl > div:nth-child(2)").css("background",color1);

				$("#modal_add_wall_preview_main").css("background","linear-gradient(280deg, "+color1+", "+ color2+ ")");
				$("#modal_add_wall_preview_main").css("background-size","400% 400%");
				$("#modal_add_wall_preview_main").css("-webkit-animation","AnimationName 59s ease infinite");
				$("#modal_add_wall_preview_main").css("-moz-animation","AnimationName 59s ease infinite");
				$("#modal_add_wall_preview_main").css("animation","AnimationName 59s ease infinite");

				$("#modal_add_wall_preview_back_type").val("anim");
				$("#modal_add_wall_preview_back_color1").val($("#modal_add_wall_settings_anim_bl > div:nth-child(2)").css("background"));
				$("#modal_add_wall_preview_back_color2").val(color2);
			}
		});

		$('#modal_add_wall_settings_anim_bl > div:nth-child(3)').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				var color1 = '#' + hex;
				var color2 = $('#modal_add_wall_settings_anim_bl > div:nth-child(2)').css('background-color');

				$("#modal_add_wall_settings_anim_bl > div:nth-child(3)").css("background",color1);

				$("#modal_add_wall_preview_main").css("background","linear-gradient(280deg, "+color1+", "+ color2+ ")");
				$("#modal_add_wall_preview_main").css("background-size","400% 400%");
				$("#modal_add_wall_preview_main").css("-webkit-animation","AnimationName 59s ease infinite");
				$("#modal_add_wall_preview_main").css("-moz-animation","AnimationName 59s ease infinite");
				$("#modal_add_wall_preview_main").css("animation","AnimationName 59s ease infinite");

				$("#modal_add_wall_preview_back_type").val("anim");
				$("#modal_add_wall_preview_back_color2").val($("#modal_add_wall_settings_anim_bl > div:nth-child(3)").css("background"));
				$("#modal_add_wall_preview_back_color1").val(color2);
			}
		});


	//--------------------------UPdate wall
		$('#modal_update_wall_settings_color_bl > div').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$('#modal_update_wall_settings_color_bl > div').css('background-color', '#' + hex);
				$('#modal_update_wall_preview_main').css('background-color', '#' + hex);
				$("#modal_update_wall_preview_back_type").val("col");
				$("#modal_update_wall_preview_back_color1").val($('#modal_update_wall_preview_main').css('background-color'));
				$("#modal_update_wall_preview_back_color2").val('');
			}
		});

		$('#modal_update_wall_settings_colortr_bl > div:nth-child(2)').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				var color1 = '#' + hex;
				var color2 = $('#modal_update_wall_settings_colortr_bl > div:nth-child(3)').css('background-color');

				$("#modal_update_wall_settings_colortr_bl > div:nth-child(2)").css("background",color1);
				$("#modal_update_wall_preview_main").css("background","linear-gradient(280deg, "+color1+", "+ color2+ ")");

				$("#modal_update_wall_preview_back_type").val("grad");
				$("#modal_update_wall_preview_back_color1").val($("#modal_update_wall_settings_colortr_bl > div:nth-child(2)").css("background"));
				$("#modal_update_wall_preview_back_color2").val(color2);
			}
		});

		$('#modal_update_wall_settings_colortr_bl > div:nth-child(3)').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				var color1 = '#' + hex;
				var color2 = $('#modal_update_wall_settings_colortr_bl > div:nth-child(2)').css('background-color');

				$("#modal_update_wall_settings_colortr_bl > div:nth-child(3)").css("background",color1);
				$("#modal_update_wall_preview_main").css("background","linear-gradient(280deg, "+color1+", "+ color2+ ")");

				$("#modal_update_wall_preview_back_type").val("grad");
				$("#modal_update_wall_preview_back_color2").val($("#modal_update_wall_settings_colortr_bl > div:nth-child(3)").css("background"));
				$("#modal_update_wall_preview_back_color1").val(color2);
			}
		});

		$('#modal_update_wall_settings_anim_bl > div:nth-child(2)').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				var color1 = '#' + hex;
				var color2 = $('#modal_update_wall_settings_anim_bl > div:nth-child(3)').css('background-color');

				$("#modal_update_wall_settings_anim_bl > div:nth-child(2)").css("background",color1);

				$("#modal_update_wall_preview_main").css("background","linear-gradient(280deg, "+color1+", "+ color2+ ")");
				$("#modal_update_wall_preview_main").css("background-size","400% 400%");
				$("#modal_update_wall_preview_main").css("-webkit-animation","AnimationName 59s ease infinite");
				$("#modal_update_wall_preview_main").css("-moz-animation","AnimationName 59s ease infinite");
				$("#modal_update_wall_preview_main").css("animation","AnimationName 59s ease infinite");

				$("#modal_update_wall_preview_back_type").val("anim");
				$("#modal_update_wall_preview_back_color1").val($("#modal_update_wall_settings_anim_bl > div:nth-child(2)").css("background"));
				$("#modal_update_wall_preview_back_color2").val(color2);
			}
		});

		$('#modal_update_wall_settings_anim_bl > div:nth-child(3)').ColorPicker({
			color: '#424242',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				var color1 = '#' + hex;
				var color2 = $('#modal_update_wall_settings_anim_bl > div:nth-child(2)').css('background-color');

				$("#modal_update_wall_settings_anim_bl > div:nth-child(3)").css("background",color1);

				$("#modal_update_wall_preview_main").css("background","linear-gradient(280deg, "+color1+", "+ color2+ ")");
				$("#modal_update_wall_preview_main").css("background-size","400% 400%");
				$("#modal_update_wall_preview_main").css("-webkit-animation","AnimationName 59s ease infinite");
				$("#modal_update_wall_preview_main").css("-moz-animation","AnimationName 59s ease infinite");
				$("#modal_update_wall_preview_main").css("animation","AnimationName 59s ease infinite");

				$("#modal_update_wall_preview_back_type").val("anim");
				$("#modal_update_wall_preview_back_color2").val($("#modal_update_wall_settings_anim_bl > div:nth-child(3)").css("background"));
				$("#modal_update_wall_preview_back_color1").val(color2);
			}
		});
	}

	$("#modal_update_wall_info > input").keyup(function(){
		$("#modal_update_wall_preview_cont > p:nth-child(1)").html($(this).val());
	});

	$("#modal_update_wall_info > textarea").keyup(function(key){
		$("#modal_update_wall_preview_cont > p:nth-child(2)").html($(this).val());
		if(key.keyCode  == 13)
		{
			var str = $("#modal_update_wall_preview_cont > p:nth-child(2)").html()+"<br />";
			$("#modal_update_wall_preview_cont > p:nth-child(2)").html(str);
		}
	});

	$("#modal_update_wall_top > div:nth-child(2) > button").click(function(){
		if($(this).html() == "Proposal" && $(this).next().attr("id") == "active_wall_upd_top_button")
		{
			$(this).attr("id","active_wall_upd_top_button");
			$(this).next().attr("id","");
			$("#modal_update_wall_top > div:nth-child(2) > input").val("proposal");
			$("#modal_update_wall_top h1").html("Add new proposal");
		}
		else if($(this).html() == "Thought" && $(this).prev().attr("id") == "active_wall_upd_top_button")
		{
			$(this).attr("id","active_wall_upd_top_button");
			$(this).prev().attr("id","");
			$("#modal_update_wall_top > div:nth-child(2) > input").val("thought");
			$("#modal_update_wall_top h1").html("Add new thought");
		}
	});

	$("#modal_update_wall_settings_type > button").click(function(){
		if($(this).attr("id") != "modal_update_wall_settings_type_active")
		{
			$("#modal_update_wall_settings_type_active").attr("id","");
			$(this).attr("id","modal_update_wall_settings_type_active");
		}

		switch($(this).html())
		{
			case 'Color':
				$(".modal_update_wall_settings_bl").css("display","none");
				$("#modal_update_wall_settings_color_bl").css("display","block");
			break;
			case 'Color transition':
				$(".modal_update_wall_settings_bl").css("display","none");
				$("#modal_update_wall_settings_colortr_bl").css("display","block");
			break;
			case 'Animation transition':
				$(".modal_update_wall_settings_bl").css("display","none");
				$("#modal_update_wall_settings_anim_bl").css("display","block");
			break;
			case 'Image':
				$(".modal_update_wall_settings_bl").css("display","none");
				$("#modal_update_wall_settings_image_bl").css("display","block");
			break;
		}
	});

	$("body").on('click',"#modal_update_wall_cards_shop",function(){
		var id = $(this).children("input").val();
		var token = $(this);
		function success(data)
		{
			$("#modal_update_wall_cards_products").html(data);
			$("#modal_update_wall_cards_products").css("display","flex");
			token.parent().css("display","none");
		}
		create_ajax(host+"/api/get_shop_cards/?get="+id+"&type=upd",success);
	});

	$("body").on("click","#modal_update_wall_cards_back", function(){
		$("#modal_update_wall_cards_products").css("display","none");
		$("#modal_update_wall_cards_shops").css("display","flex");
	});

	$("body").on("click","#modal_update_wall_cards_product",function(){
		var id = $(this).children("input").val();

		function success(data)
		{
			var is_id = 0;
			var spl_id = $("#modal_update_wall_preview_cards_count").val().split(",");
			if(spl_id.length > 0)
			{
				for(var i = 0; i < spl_id.length; i++)
				{
					if(id == spl_id[i])
					{
						is_id = 1;
					}
				}
				if(is_id == 0)
				{
					if(spl_id.length <=4)
					{
						var html = $("#modal_update_wall_preview_cont_cards").html()+data;
						$("#modal_update_wall_preview_cont_cards").html(html);
						$("#modal_update_wall_preview_cards_count").val($("#modal_update_wall_preview_cards_count").val()+id+',');
					}
				}
			}
			else
			{
				if(spl_id.length <=4)
				{
					var html = $("#modal_update_wall_preview_cont_cards").html()+data;
					$("#modal_update_wall_preview_cont_cards").html(html);
					$("#modal_update_wall_preview_cards_count").val($("#modal_add_wall_preview_cards_count").val()+id+',');
				}
			}
		}
		create_ajax(host+"/api/get_shop_cards/?add="+id+"&type=upd",success);
	});

	$("body").on('dblclick','#my_page_menu > ul > li',function() {
		var id = $(this).next().val();

		function success(data)
		{
			var dat = JSON.parse(data);
			$("#modal_update_wall_settings_type > button").attr("id","");

			switch(dat.type)
      {
      	case 'anim':
      		$(".modal_update_wall_settings_bl").css("display","none");
      		$("#modal_update_wall_settings_anim_bl").css("display","block");
      		$("#modal_update_wall_settings_type > button:nth-child(3)").attr("id","modal_update_wall_settings_type_active");
      		$("#modal_update_wall_preview_main").html("");
      		document.getElementById('modal_update_wall_preview_main').style.background = dat.setting;
      		$("#modal_update_wall_preview_main").css({'background-size': '400% 400%', '-webkit-animation': 'AnimationName 59s ease infinite', 'animation': 'AnimationName 59s ease infinite', '-moz-animation': 'AnimationName 59s ease infinite'})
      		var color = dat.setting.split(',');
      		color[2] = color[2].slice(0, color[2].length-1);
      		$("#modal_update_wall_settings_anim_bl > div:nth-child(2)").css("background",color[1]);
      		$("#modal_update_wall_settings_anim_bl > div:nth-child(3)").css("background",color[2]);
      	break;
      	case 'grad':
      		$(".modal_update_wall_settings_bl").css("display","none");
      		$("#modal_update_wall_settings_colortr_bl").css("display","block");
      		$("#modal_update_wall_settings_type > button:nth-child(2)").attr("id","modal_update_wall_settings_type_active");
      		document.getElementById('modal_update_wall_preview_main').style.background = 'transparent';
      		$("#modal_update_wall_preview_main").css({'background-size': '', '-webkit-animation': '', 'animation': '', '-moz-animation': ''})
      		document.getElementById('modal_update_wall_preview_main').style.background = dat.setting;
      		$("#modal_update_wall_preview_main").html("");
      		var color = dat.setting.split(',');
      		color[2] = color[2].slice(0, color[2].length-1);
      		$("#modal_update_wall_settings_colortr_bl > div:nth-child(2)").css("background",color[1]);
      		$("#modal_update_wall_settings_colortr_bl > div:nth-child(3)").css("background",color[2]);
      	break;
      	case 'col':
      		$(".modal_update_wall_settings_bl").css("display","none");
      		$("#modal_update_wall_settings_color_bl").css("display","block");
      		$("#modal_update_wall_settings_type > button:nth-child(1)").attr("id","modal_update_wall_settings_type_active");
      		document.getElementById('modal_update_wall_preview_main').style.background = 'transparent';
      		$("#modal_update_wall_preview_main").css({'background-size': '', '-webkit-animation': '', 'animation': '', '-moz-animation': ''})
      		document.getElementById('modal_update_wall_preview_main').style.background = dat.setting;
      		$("#modal_update_wall_settings_color_bl div").css("background",dat.setting);
      		$("#modal_update_wall_preview_main").html("");
      	break;
      	case 'img':
      		$(".modal_update_wall_settings_bl").css("display","none");
      		$("#modal_update_wall_settings_image_bl").css("display","block");
      		$("#modal_update_wall_settings_type > button:nth-child(4)").attr("id","modal_update_wall_settings_type_active");
      		document.getElementById('modal_update_wall_preview_main').style.background = 'transparent';
      		$("#modal_update_wall_preview_main").html(dat.setting);
      	break;
      }

      $("#modal_update_wall_preview_cont > p:nth-child(1)").html(dat.content.title);
      $("#modal_update_wall_preview_cont > p:nth-child(2)").html(dat.content.descr);

      $("#modal_update_wall_info input").val(dat.content.title);
      $("#modal_update_wall_info textarea").html(dat.content.descr);
      $("#modal_update_wall_id").val(dat.wall_id);

			if(dat.cards == 1)
			{
				$("#modal_update_wall_preview_cont_cards").html(dat.html);
			}
		}
		create_ajax(host+"/api/show_wall/?update="+id,success);
	});

	$("#modal_update_wall_top button:nth-child(3)").click(function(){
		var id = $("#modal_update_wall_id").val();
		var user = $("#user_id").val();
		var title = $("#modal_update_wall_info > input").val();
		var descr = $("#modal_update_wall_info > textarea").val();
		var type = "proposal";

		var color_type = $("#modal_update_wall_preview_back_type").val();
		var color1 = $("#modal_update_wall_preview_back_color1").val();
		var color2_spl = $("#modal_update_wall_preview_back_color2").val().split(" none");
		var color2 = color2_spl[0];
		var color = '';

		switch(color_type)
		{
			case 'col':
				color = color1;
			break;
			case 'grad':
				color = 'linear-gradient(270deg, '+color1+', '+color2+')';
			break;
			case 'anim':
				color = 'linear-gradient(270deg, '+color1+', '+color2+')';
			break;
		}

		var spl_cards = $("#modal_update_wall_preview_cards_count").val();
		var cards = spl_cards.slice(0, spl_cards.length-1);
		var link = host+"/api/update_wall/?"+id+"="+id+"&"+title+"="+descr+"&"+color_type+"="+color+"&id="+cards;

		function success(data)
		{
			console.log(data);
		}
		create_ajax(link,success);
	});
	/*----------------------------------------------------------------*/

	$("#modal_add_wall_info > input").keyup(function(){
		$("#modal_add_wall_preview_cont > p:nth-child(1)").html($(this).val());
	});

	$("#modal_add_wall_info > textarea").keyup(function(key){
		$("#modal_add_wall_preview_cont > p:nth-child(2)").html($(this).val());
		if(key.keyCode  == 13)
		{
			var str = $("#modal_add_wall_preview_cont > p:nth-child(2)").html()+"<br />";
			$("#modal_add_wall_preview_cont > p:nth-child(2)").html(str);
		}
	});

	$("#modal_add_wall_top > div:nth-child(2) > button").click(function(){
		if($(this).html() == "Proposal" && $(this).next().attr("id") == "active_wall_top_button")
		{
			$(this).attr("id","active_wall_top_button");
			$(this).next().attr("id","");
			$("#modal_add_wall_top > div:nth-child(2) > input").val("proposal");
			$("#modal_add_wall_top h1").html("Add new proposal");
		}
		else if($(this).html() == "Thought" && $(this).prev().attr("id") == "active_wall_top_button")
		{
			$(this).attr("id","active_wall_top_button");
			$(this).prev().attr("id","");
			$("#modal_add_wall_top > div:nth-child(2) > input").val("thought");
			$("#modal_add_wall_top h1").html("Add new thought");
		}
	});

	$("#modal_add_wall_settings_type > button").click(function(){
		if($(this).attr("id") != "modal_add_wall_settings_type_active")
		{
			$("#modal_add_wall_settings_type_active").attr("id","");
			$(this).attr("id","modal_add_wall_settings_type_active");
		}

		switch($(this).html())
		{
			case 'Color':
				$(".modal_add_wall_settings_bl").css("display","none");
				$("#modal_add_wall_settings_color_bl").css("display","block");
			break;
			case 'Color transition':
				$(".modal_add_wall_settings_bl").css("display","none");
				$("#modal_add_wall_settings_colortr_bl").css("display","block");
			break;
			case 'Animation transition':
				$(".modal_add_wall_settings_bl").css("display","none");
				$("#modal_add_wall_settings_anim_bl").css("display","block");
			break;
			case 'Image':
				$(".modal_add_wall_settings_bl").css("display","none");
				$("#modal_add_wall_settings_image_bl").css("display","block");
			break;
		}
	});

	$("body").on('click',"#modal_add_wall_cards_shop",function(){
		var id = $(this).children("input").val();
		var token = $(this);
		function success(data)
		{
			$("#modal_add_wall_cards_products").html(data);
			$("#modal_add_wall_cards_products").css("display","flex");
			token.parent().css("display","none");
		}
		create_ajax(host+"/api/get_shop_cards/?get="+id+"&type=add",success);
	});

	$("body").on("click","#modal_add_wall_cards_back", function(){
		$("#modal_add_wall_cards_products").css("display","none");
		$("#modal_add_wall_cards_shops").css("display","flex");
	});

	$("body").on("click","#modal_add_wall_cards_product",function(){
		var id = $(this).children("input").val();

		function success(data)
		{
			var is_id = 0;
			var spl_id = $("#modal_add_wall_preview_cards_count").val().split(",");
			if(spl_id.length > 0)
			{
				for(var i = 0; i < spl_id.length; i++)
				{
					if(id == spl_id[i])
					{
						is_id = 1;
					}
				}
				if(is_id == 0)
				{
					if(spl_id.length <=4)
					{
						var html = $("#modal_add_wall_preview_cont_cards").html()+data;
						$("#modal_add_wall_preview_cont_cards").html(html);
						$("#modal_add_wall_preview_cards_count").val($("#modal_add_wall_preview_cards_count").val()+id+',');
					}
				}
			}
			else
			{
				if(spl_id.length <=4)
				{
					var html = $("#modal_add_wall_preview_cont_cards").html()+data;
					$("#modal_add_wall_preview_cont_cards").html(html);
					$("#modal_add_wall_preview_cards_count").val($("#modal_add_wall_preview_cards_count").val()+id+',');
				}
			}
		}
		create_ajax(host+"/api/get_shop_cards/?add="+id+"&type=add",success);
	});

	$("#modal_add_wall_top > button").click(function(){
		var user = $("#user_id").val();
		var title = $("#modal_add_wall_info > input").val();
		var descr = $("#modal_add_wall_info > textarea").val();
		var type = "proposal";

		var color_type = $("#modal_add_wall_preview_back_type").val();
		var color1 = $("#modal_add_wall_preview_back_color1").val();
		var color2_cpl = $("#modal_add_wall_preview_back_color2").val().split(' none');
		var color2 = color2_cpl[0];
		var color = '';

		switch(color_type)
		{
			case 'col':
				color = color1;
			break;
			case 'grad':
				color = 'linear-gradient(270deg, '+color1+', '+color2+')';
			break;
			case 'anim':
				color = 'linear-gradient(270deg, '+color1+', '+color2+')';
			break;
		}

		var spl_cards = $("#modal_add_wall_preview_cards_count").val();
		var cards = spl_cards.slice(0, spl_cards.length-1);
		var link = host+"/api/add_wall/?"+user+"="+user+"&"+title+"="+descr+"&"+color_type+"="+color+"&cards="+cards;

		function success(data)
		{
			window.location.reload();
		}
		create_ajax(link,success);
	});

	$("#modal_update_wall_top > button:nth-child(4)").click(function(){
		var id = $("#modal_update_wall_id").val();
		function success(data)
		{
			console.log(data);
			window.location.reload();
		}
		create_ajax(host+"/api/delete_wall/?id="+id,success);
	});
});