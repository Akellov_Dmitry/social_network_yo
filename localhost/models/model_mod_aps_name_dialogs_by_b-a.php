<?php
class Model_dialogs extends Model
{
	public function get_data()
	{
		$data = array();
		
		$urls = array();

		$res = multi_thread_request(array(__NAME__."api/get_user_info/?".$_COOKIE['uid']."=".$_COOKIE['uid']."&act=friends",__NAME__."api/get_user_dialogs/?id=".$_COOKIE['uid']));

		$data = json_decode($res[__NAME__."api/get_user_info/?".$_COOKIE['uid']."=".$_COOKIE['uid']."&act=friends"],true);
		$data["dialogs"] = json_decode($res[__NAME__."api/get_user_dialogs/?id=".$_COOKIE['uid']],true);

		return $data;
	}
}
?>