<?php
class Model_shop extends Model
{
	public function get_data( $param = null )
	{
		$data = array();
		
		$urls = array();
		$res = multi_thread_request(array(__NAME__."api/get_user_info/?".$_COOKIE['uid']."=".$_COOKIE['uid']."&act=", __NAME__."api/get_shop_prod/?id=".$param, __NAME__."api/get_cats/?id=".$param,__NAME__."api/get_cart/?id=".$_COOKIE['uid']));

		$data = json_decode($res[__NAME__."api/get_user_info/?".$_COOKIE['uid']."=".$_COOKIE['uid']."&act="],true);
		$data["shop"] = json_decode($res[__NAME__."api/get_shop_prod/?id=".$param],true);
		$data["cats"] = json_decode($res[__NAME__."api/get_cats/?id=".$param],true);
		$data["cart"] = json_decode($res[__NAME__."api/get_cart/?id=".$_COOKIE['uid']],true);

		return $data;
	}
}
?>