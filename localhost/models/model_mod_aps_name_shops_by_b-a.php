<?php
class Model_shops extends Model
{
	public function get_data()
	{
		$data = array();
		
		$urls = array();

		$res = multi_thread_request(array(__NAME__."api/get_user_info/?".$_COOKIE['uid']."=".$_COOKIE['uid']."&act=", __NAME__."api/get_user_shops/?id=".$_COOKIE['uid'],__NAME__."api/get_cart/?id=".$_COOKIE['uid']));

		$data = json_decode($res[__NAME__."api/get_user_info/?".$_COOKIE['uid']."=".$_COOKIE['uid']."&act="],true);
		$data["shops"] = json_decode($res[__NAME__."api/get_user_shops/?id=".$_COOKIE['uid']],true);
		$data["cart"] = json_decode($res[__NAME__."api/get_cart/?id=".$_COOKIE['uid']],true);

		return $data;
	}
}
?>