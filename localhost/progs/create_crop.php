<?php
	require_once '../database/DB.php';

	$folder = '../img/crop/';
	$filename = $_POST['filename'];

	$targ_w = 120;
	$targ_h = 100;

	$ratio = $targ_w / $targ_h;

	$src = imagecreatefromjpeg($folder.$filename);

	$tmp = imagecreatetruecolor($targ_w, $targ_h);
	imagecopyresampled($tmp,$src,0,0,$_POST['x'],$_POST['y'],$targ_w, $targ_h,$_POST['w'],$_POST['h']);
	imagejpeg($tmp, $folder.$_COOKIE['uid'].'-av-'.$filename,100);

	DB::getInstance() -> no_returns("UPDATE us_main SET sys_avatar = 'crop/".$_COOKIE['uid'].'-av-'.$filename."' WHERE id = ".$_COOKIE["uid"]);

	imagedestroy($tmp);
	imagedestroy($src);

	echo "OK";