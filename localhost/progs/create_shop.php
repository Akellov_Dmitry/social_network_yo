<?php
	require_once '../progs/functions.php';
	require_once '../database/DB.php';

	$id = $_POST['id'];
	$name = addslashes(htmlspecialchars(urldecode($_POST['name'])));
	$descr = addslashes(htmlspecialchars(urldecode($_POST['descr'])));


	$folder = "../img/shops_logo/";
	$orig_w = 300;

	$imageFile = $_FILES['file-0']['tmp_name'];
	$filename = basename($_FILES['file-0']['name']);

	$to_db = $id.'-'.$filename;

	$valid = DB::getInstance() -> query("SELECT id FROM us_shops WHERE name = '".$name."'");
	
	if($valid -> count() == 0)
	{
		DB::getInstance() -> no_returns("INSERT INTO us_shops(name,logo,descr)VALUES(
				'".$name."',
				'".$to_db."',
				'".$descr."'
			)");

		list($width, $height ) = getimagesize($imageFile);

		$src = imagecreatefromjpeg($imageFile);
		$orig_h = ($height/$width)*$orig_w;

		$tmp = imagecreatetruecolor($orig_w, $orig_h);
		imagecopyresampled($tmp,$src,0,0,0,0,$orig_w, $orig_h,$width,$height);
		imagejpeg($tmp, $folder.$id.'-'.$filename,100);

		imagedestroy($tmp);
		imagedestroy($src);

		$filename = urlencode($filename);
		
		mkdir('../img/products/'.str2url($name));	

		$id_query = DB::getInstance() -> query("SELECT id FROM us_shops WHERE logo='".$to_db."' ORDER BY id DESC LIMIT 1");

		DB::getInstance() -> query("INSERT INTO shops_relations(user_id,shop_id,type)VALUES(
			'".$id."',
			".$id_query -> results()[0] -> id.",
			'admin'
		)");
		echo $id_query -> results()[0] -> id;
	}
	else
	{
		echo "name!";
	}
?>