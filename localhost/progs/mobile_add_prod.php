<?php
 
    require_once '../progs/functions.php';
    require_once '../database/DB.php';
 
    $json = json_decode(file_get_contents('php://input'),true);
   
    $id = $json["id"];
    $name = $json["name"];
    $cost = $json["cost"];
    $descr = $json["descr"];
    $image = $json["image"];
    
    $shop = DB::getInstance() -> query("SELECT name FROM us_shops WHERE id = ".$id);
    $folder = "../img/products/".$shop->results()[0] -> name."/";

    $response = array();

    $to_db = mb_strtolower($id).'-'.mb_strtolower($name).".jpg";

    $decodedImage = base64_decode("$image");

    $return = file_put_contents($folder.mb_strtolower($to_db), $decodedImage);  

    if($return !== false){
        $id_query = DB::getInstance() -> no_returns("INSERT INTO sh_product(name,descr,cost,photo,shop_id)VALUES(
                '".$name."',
                '".$descr."',
                '".$cost."',
                '".$to_db."',
                ".$id."
            )");

        $response['success'] = 1;
        $response['message'] = $id_query -> results()[0] -> id;
    }else{
        $response['success'] = 0;
        $response['message'] = "Image Uploaded Failed";
    }
?>