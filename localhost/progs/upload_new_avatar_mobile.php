<?php
 
    require_once '../progs/functions.php';
    require_once '../database/DB.php';
 
    $jsonProd = json_decode(file_get_contents('php://input'),true);
   
    $name = $jsonProd["name"];
    $id = $jsonProd["id"];
    $image = $jsonProd["image"];
    
    $response = array();

    $to_db = mb_strtolower($id).'-'.mb_strtolower($name).".jpg";

    $decodedImage = base64_decode("$image");

    $return = file_put_contents("../img/".mb_strtolower($to_db), $decodedImage);  

    if($return !== false){
        $id_query = DB::getInstance() -> no_returns("UPDATE us_main SET sys_avatar = ".$to_db."' WHERE id = ".$id]);

        $response['success'] = 1;
        $response['message'] = $id_query -> results()[0] -> id;
    }else{
        $response['success'] = 0;
        $response['message'] = "Image Uploaded Failed";
    }
?>