<?php
	$folder = '../img/crop/';
	$filename = $_POST['filename'];
	$orig_w = 150;
	$orig_h = $_POST['height'];

	$targ_w = 120;
	$targ_h = 100;

	$ratio = $targ_w / $targ_h;
?>
	<script type="text/javascript">
	  $(function(){
	  	$("#cropbox").Jcrop({
	  		aspectRatio: <?php echo $ratio?>,
	  		setSelect: [0,0,<?php echo $orig_w.','.$orig_h;?>],
	  		onSelect: updateCoords,
	  		onChange: updateCoords
	  	});
	  });

	  function updateCoords(c)
	  {
	  	showPreview(c);
	  	$("#page_add_avatar_modal_avatar_submit_crop_x").val(c.x);
	  	$("#page_add_avatar_modal_avatar_submit_crop_y").val(c.y);
	  	$("#page_add_avatar_modal_avatar_submit_crop_w").val(c.w);
	  	$("#page_add_avatar_modal_avatar_submit_crop_h").val(c.h);
	  }

	  function showPreview(coords)
	  {
	  	var rx = <?php echo $targ_w; ?> / coords.w;
	  	var ry = <?php echo $targ_h; ?> / coords.h;

	  	$("#preview img").css({
	  		width: Math.round(rx*<?php echo $orig_w; ?>)+'px',
	  		height: Math.round(ry*<?php echo $orig_h; ?>)+'px',
	  		marginLeft: '-'+ Math.round(rx*coords.x)+'px',
	  		marginTop: '-' + Math.round(ry*coords.y)+'px',
	  	});
	  }
	</script>
	<style type="text/css">	
		#preview
		{
			width: <?php echo $targ_w; ?>px;
			height: <?php echo $targ_h; ?>px;
			overflow: hidden;
		}
	</style>
	<h1>Cropping img</h1>

	<table>
		<tr>
			<td>
				<input type="hidden" id="page_add_avatar_modal_avatar_filename" value="<?php echo $filename; ?>">
				<img src="<?php echo $folder.$filename ?>" id="cropbox" alt="<?php echo $folder.$filename ?>" />
			</td>
			<td>
				Thumb preview
				<div id="preview">
					<img src="<?php echo $folder.$filename ?>" id="previewImg" alt="thumb" />
				</div>
			</td>
		</tr>
	</table>
		<p>
			<input type="hidden" id="page_add_avatar_modal_avatar_submit_crop_x" value="0" />
			<input type="hidden" id="page_add_avatar_modal_avatar_submit_crop_y" />
			<input type="hidden" id="page_add_avatar_modal_avatar_submit_crop_w" />
			<input type="hidden" id="page_add_avatar_modal_avatar_submit_crop_h" />
			<input type="button" id="page_add_avatar_modal_avatar_submit_crop_button" value="Crop image" />
		</p>
	</form>