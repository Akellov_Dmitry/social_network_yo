<?php
	require_once '../progs/functions.php';
	require_once '../database/db.php';

	$id = $_POST['id'];
	$name = addslashes(htmlspecialchars(urldecode($_POST['name'])));
	$descr = addslashes(htmlspecialchars(urldecode($_POST['descr'])));


	$folder = "../img/shops_logo/";
	$orig_w = 300;

	$imageFile = $_FILES['file-0']['tmp_name'];
	$filename = basename($_FILES['file-0']['name']);

	$to_db = $id.'-'.$filename;

	DB::getInstance() -> no_returns("UPDATE us_shops SET name = '".$name."' WHERE id = ".$id);
	DB::getInstance() -> no_returns("UPDATE us_shops SET descr = '".$descr."' WHERE id = ".$id);

	if($filename != "")
	{
		$img = DB::getInstance() -> query("SELECT logo FROM us_shops WHERE logo = ".$id);
		DB::getInstance() -> no_returns("UPDATE us_shops SET logo = '".$to_db."' WHERE id = ".$id);
		
		unlink($folder.$img->results()[0] -> logo);
		list($width, $height ) = getimagesize($imageFile);

		$src = imagecreatefromjpeg($imageFile);
		$orig_h = ($height/$width)*$orig_w;

		$tmp = imagecreatetruecolor($orig_w, $orig_h);
		imagecopyresampled($tmp,$src,0,0,0,0,$orig_w, $orig_h,$width,$height);
		imagejpeg($tmp, $folder.$id.'-'.$filename,100);

		imagedestroy($tmp);
		imagedestroy($src);

		$filename = urlencode($filename);
	}
?>