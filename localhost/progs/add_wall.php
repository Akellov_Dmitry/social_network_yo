<?php
  require_once '../progs/functions.php';
  require_once '../database/DB.php';

  $type = $_POST['type'];
  $id = addslashes(htmlspecialchars(urldecode($_POST['user_id'])));
  $wall_id = addslashes(htmlspecialchars(urldecode($_POST['wall_id'])));


  $folder = "../img/imgs/";
  $orig_w = 300;

  $imageFile = $_FILES['file-0']['tmp_name'];
  $filename = basename($_FILES['file-0']['name']);

  $to_db = $id.'-'.$filename;

  switch($type)
  {
    case 'add':
      DB::getInstance()->no_returns("INSERT INTO wall(user_id,posted_id) VALUES(
          '".$id."',
          '".$id."'
        )");
      DB::getInstance()->no_returns("INSERT INTO wall_img(name,wall_id) VALUES(
          '".$id.'-'.$filename."',
          '".$id."'
        )");
    break;
    case 'repost':
      DB::getInstance()->no_returns("INSERT INTO wall(user_id,posted_id) VALUES(
          '".$id."',
          '".$wall_id."'
        )");
    break;
  }

    list($width, $height ) = getimagesize($imageFile);

    $src = imagecreatefromjpeg($imageFile);
    $orig_h = ($height/$width)*$orig_w;

    $tmp = imagecreatetruecolor($orig_w, $orig_h);
    imagecopyresampled($tmp,$src,0,0,0,0,$orig_w, $orig_h,$width,$height);
    imagejpeg($tmp, $folder.$id.'-'.$filename,100);

    imagedestroy($tmp);
    imagedestroy($src);

    $filename = urlencode($filename);

    echo "OK";
?>