<?php
class Controller_shop extends Controller
{
	function __construct()
	{
		$this -> model = new Model_shop();
		$this -> view = new View();
	}

	function action_default($param)
	{
		$data = $this -> model -> get_data($param[0]);
		$this -> view -> generate("shop_view.php", "main.php", $data);
	}
}
?>