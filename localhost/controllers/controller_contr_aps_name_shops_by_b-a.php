<?php
class Controller_shops extends Controller
{
	function __construct()
	{
		$this -> model = new Model_shops();
		$this -> view = new View();
	}

	function action_default()
	{
		$data = $this -> model -> get_data();
		$this -> view -> generate("shops_view.php", "main.php", $data);
	}
}
?>