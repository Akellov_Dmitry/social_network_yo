<title>Друзья</title>
<div id="friends_cont">
	<div id="friends_cont_menu">
		<ul>
			<li class="active">Following: <span><?php echo $data['user_friends_count']; ?></span></li>
			<li class="active">Followers</span></li>
			<li>Search friends</li>
			<li>Black list: <span>0</span></li>
		</ul>
	</div>
	<div id="friends_view" class="active">
		<div id="friends_stat">
			<label>Count of people: </label><label><?php echo $data['user_friends_count']; ?></label>
		</div>
		<ul>

		<?php for( $i = 0; $i < $data['user_friends_count']; $i++ ){ ?>
			<li>
				<div>
				<i class="fas fa-user-slash" title="To black list"></i>
				<?php if($data[$i]['user_friends_avatar'] != ""){ ?>
						<a href='/my_page/user/<?php echo $data[$i]['user_friends_id'] ?>'><img src='../../img/<?php echo $data[$i]['user_friends_avatar'] ?>' /></a>
					<?php }else{ ?>
						<a href='/my_page/user/<?php echo $data[$i]['user_friends_id'] ?>'><img src='../../img/no_photo.jpg' /></a>
					<?php } ?>
					<i class="fas fa-user-times" title="Delete"></i>
					<a href='/my_page/user/<?php echo $data[$i]['user_friends_id'] ?>'><?php echo $data[$i]['user_friends_name'] ?> <?php echo $data[$i]['user_friends_lastname'] ?></a>		
					<div>
						<div>
							<i class="far fa-star"></i>
							<label><?php echo $data[$i]['user_friends_b_rank']; ?></label>
						</div>
						<div>
							<i class="fas fa-shopping-cart"></i>
							<label><?php echo $data[$i]['user_friends_shops']; ?></label>
						</div>
						<div>
							<i class="fas fa-users"></i>
							<label><?php echo $data[$i]['user_friends_fr']; ?></label>
						</div>
					</div>
					</div>
					<div>
					<i class='far fa-comment' title='Write message'></i>
					<i class="fa fa-phone" title="Audiocall to user"></i>
					<i class="fa fa-video" title="Videocall to user"></i>
					<input type="hidden" value="<?php echo $data[$i]['user_friends_id'] ?>"></div>
			</li>
		<?php } ?>
	</div>
	<div id="followers_view">
		<div id="followers_stat">
			<label>Count of people: </label><label><?php echo $data['user_followers_count']; ?></label>
		</div>
		<ul>

		<?php for( $i = 0; $i < $data['user_followers_count']; $i++ ){ ?>
			<li>
				<div>
				<i class="fas fa-user-slash" title="To black list"></i>
				<?php if($data["follower-$i"]['user_followers_avatar'] != ""){ ?>
						<a href='/my_page/user/<?php echo $data["follower-$i"]['user_followers_id'] ?>'><img src='../../img/<?php echo $data["follower-$i"]['user_followers_avatar'] ?>' /></a>
					<?php }else{ ?>
						<a href='/my_page/user/<?php echo $data["follower-$i"]['user_followers_id'] ?>'><img src='../../img/no_photo.jpg' /></a>
					<?php } ?>
					<i class="fas fa-user-times" title="Delete"></i>
					<a href='/my_page/user/"<?php echo $data["follower-$i"]['user_followers_id'] ?>'><?php echo $data["follower-$i"]['user_followers_name'] ?> <?php echo $data["follower-$i"]['user_followers_lastname'] ?></a>
					<div>
						<div>
							<i class="far fa-star"></i>
							<label><?php echo $data["follower-$i"]['user_followers_b_rank']; ?></label>
						</div>
						<div>
							<i class="fas fa-shopping-cart"></i>
							<label><?php echo $data["follower-$i"]['user_followers_shops']; ?></label>
						</div>
						<div>
							<i class="fas fa-users"></i>
							<label><?php echo $data["follower-$i"]['user_followers_fr']; ?></label>
						</div>
					</div>
					</div>
					<div>
						<i class='far fa-comment' title='Write message'></i>
						<i class="fa fa-phone" title="Audiocall to user"></i>
						<i class="fa fa-video" title="Videocall to user"></i>
						<input type="hidden" value="<?php echo $data["follower-$i"]['user_followers_id'] ?>">
					</div>
			</li>
		<?php } ?>
	</div>
	<div id="search_friends_view">
		<div id="search_friends_input_form">
			<input type="text" />
			<i class="fa fa-search"></i>
			<label>Name or surname...</label>
			<div id="search_friends_input_form_line"></div>
		</div>
		<div id="search_friends_stat">
			<label>Count of people: </label><label>0</label>
		</div>
		<div id="search_friends_result">
			<svg class="friends_search_spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
			   <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
		</div>
	</div>

	<div id="black_list_view">
		<div id="black_list_stat">
			<label>In black list: </label><label><?php echo $data['user_black_list_count']; ?></label>
		</div>
		<ul>

		<?php for( $i = 0; $i < $data['user_black_list_count']; $i++ ){ ?>
			<li>
				<div>
				<i class="fas fa-user-slash" title="To black list"></i>
				<?php if($data["black_list-$i"]['user_black_list_avatar'] != ""){ ?>
						<a href='/my_page/user/<?php echo $data["black_list-$i"]['user_black_list_id'] ?>'><img src='../../img/<?php echo $data["black_list-$i"]['user_black_list_avatar'] ?>' /></a>
					<?php }else{ ?>
						<a href='/my_page/user/<?php echo $data["black_list-$i"]['user_black_list_id'] ?>'><img src='../../img/no_photo.jpg' /></a>
					<?php } ?>
					<i class="fas fa-user-times" title="Delete"></i>
					<a href='/my_page/user/"<?php echo $data["black_list-$i"]['user_black_list_id'] ?>'><?php echo $data["black_list-$i"]['user_black_list_name'] ?> <?php echo $data["black_list-$i"]['user_black_list_lastname'] ?></a>
					<div>
						<div>
							<i class="far fa-star"></i>
							<label><?php echo $data["black_list-$i"]['user_black_list_b_rank']; ?></label>
						</div>
						<div>
							<i class="fas fa-shopping-cart"></i>
							<label><?php echo $data["black_list-$i"]['user_black_list_shops']; ?></label>
						</div>
						<div>
							<i class="fas fa-users"></i>
							<label><?php echo $data["black_list-$i"]['user_black_list_fr']; ?></label>
						</div>
					</div>
					</div>
					<div>
					<i class='far fa-comment' title='Write message'></i>
					<i class="fa fa-phone" title="Audiocall to user"></i>
					<i class="fa fa-video" title="Videocall to user"></i>
					<input type="hidden" value="<?php echo $data["black_list-$i"]['user_black_list_id'] ?>"></div>
			</li>
		<?php } ?>
	</div>
</div>