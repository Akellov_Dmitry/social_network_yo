<?php
$data = array();
$user_fr_single;
$user_fr = array();
$user_fr_main;

if($params_name[0] == $params_value[0])
{	
	$users = DB::getInstance() -> query('SELECT * FROM us_main WHERE unique_id = "'.$params_value[0].'"');
	$data['error'] = false;
	$data['my_info'] = array(
								  'id' => $users -> results()[0] -> unique_id,
								  'sys_email' => $users -> results()[0] -> sys_email,
								  'sys_telephone' => $users -> results()[0] -> sys_telephone,
								  'sys_link' => $users -> results()[0] -> sys_link,
								  'sys_avatar' => $users -> results()[0] -> sys_avatar,
								  'main_name' => $users -> results()[0] -> main_name,
								  'main_lastname' => $users -> results()[0] -> main_lastname,
								  'sys_status' => $users -> results()[0] -> sys_status,
								  'sys_priv' => $users -> results()[0] -> sys_priv,
								  'b_rank' => $users -> results()[0] -> b_rank,
								  'b_rate' => $users -> results()[0] -> b_rate,
								  'main_status' => $users -> results()[0] -> main_status
								);
	$counterr = 1;

	switch($params_value[1])
	{
		case 'friends':
			$following = DB::getInstance() -> query("SELECT user2 FROM followers WHERE user1='".$params_name[0]."'");
			$following_count = $following->count();
			$counterr += $following_count;
			$counterrrr = 0;
			$data['user_friends_count'] = $following_count;

			for($i = 0; $i < $following_count; $i++)
			{
				$user_fr_main = DB::getInstance() -> query('SELECT unique_id,main_lastname,main_name,sys_avatar,main_status,b_rank,b_rate FROM us_main WHERE unique_id="'.$following->results()[$counterr-$following_count-$i-$i] -> user2.'"');
				
				$data[$i]['user_friends_id'] = $user_fr_main -> results()[$counterr] -> unique_id;
				$data[$i]['user_friends_name'] = $user_fr_main -> results()[$counterr] -> main_name;
				$data[$i]['user_friends_lastname'] = $user_fr_main -> results()[$counterr] -> main_lastname;
				$data[$i]['user_friends_avatar'] = $user_fr_main -> results()[$counterr] -> sys_avatar;
				$data[$i]['user_friends_status'] = $user_fr_main -> results()[$counterr] -> main_status;
				$data[$i]['user_friends_b_rank'] = $user_fr_main -> results()[$counterr] -> b_rank;
				$data[$i]['user_friends_b_rate'] = $user_fr_main -> results()[$counterr] -> b_rate;

				$shops = DB::getInstance() -> query("SELECT COUNT(*) AS shops FROM shops_relations WHERE user_id = '".$following->results()[$counterr-$following_count-$i-$i] -> user2."'");

				$fr = DB::getInstance() -> query("SELECT COUNT(*) AS friends FROM followers WHERE user1 = '".$following->results()[$counterr-$following_count-$i-$i] -> user2."'");

				$counterr++;
				$data[$i]['user_friends_shops'] = $shops -> results()[$counterr] -> shops;
				$counterr ++;


				$data[$i]['user_friends_fr'] = $fr -> results()[$counterr] -> friends;
				
				$counterr ++;
				$counterrrr = $i+$i;
			}

			$followers = DB::getInstance() -> query("SELECT user1 FROM followers WHERE user2='".$params_name[0]."'");
			$followers_count = $followers->count();
			$counterr += $followers_count;
			$data['user_followers_count'] = $followers_count;

			for($i = 0; $i < $followers_count; $i++)
			{
				$user_fr_main = DB::getInstance() -> query('SELECT unique_id,main_lastname,main_name,sys_avatar,main_status,b_rank,b_rate FROM us_main WHERE unique_id="'.$followers->results()[$counterr-$followers_count-$i-$i] -> user1.'"');

				$data["follower-".$i]['user_followers_id'] = $user_fr_main -> results()[$counterr] -> unique_id;
				$data["follower-".$i]['user_followers_name'] = $user_fr_main -> results()[$counterr] -> main_name;
				$data["follower-".$i]['user_followers_lastname'] = $user_fr_main -> results()[$counterr] -> main_lastname;
				$data["follower-".$i]['user_followers_avatar'] = $user_fr_main -> results()[$counterr] -> sys_avatar;
				$data["follower-".$i]['user_followers_status'] = $user_fr_main -> results()[$counterr] -> main_status;
				$data["follower-".$i]['user_followers_b_rank'] = $user_fr_main -> results()[$counterr] -> b_rank;
				$data["follower-".$i]['user_followers_b_rate'] = $user_fr_main -> results()[$counterr] -> b_rate;
				
				$shops = DB::getInstance() -> query("SELECT COUNT(*) AS shops FROM shops_relations WHERE user_id = '".$following->results()[$counterr-$followers_count-$i-$i] -> user1."'");

				$fr = DB::getInstance() -> query("SELECT COUNT(*) AS friends FROM followers WHERE user1 = '".$following->results()[$counterr-$followers_count-$i-$i] -> user1."'");
				
				$counterr++;
				$data["follower-".$i]['user_followers_shops'] = $shops -> results()[$counterr] -> shops;
				$counterr ++;


				$data["follower-".$i]['user_followers_fr'] = $fr -> results()[$counterr] -> friends;
				
				$counterr ++;
			}

			$black_list = DB::getInstance() -> query("SELECT user2 FROM black_list WHERE user1='".$params_name[0]."'");
			$black_list_count = $black_list->count();
			$counterr += $black_list_count;
			$data['user_black_list_count'] = $black_list_count;

			for($i = 0; $i < $black_list_count; $i++)
			{
				$user_fr_main = DB::getInstance() -> query('SELECT unique_id,main_lastname,main_name,sys_avatar,main_status,b_rank,b_rate FROM us_main WHERE unique_id="'.$black_list->results()[$counterr-$black_list_count-$i-$i] -> user2.'"');

				$data["black_list-".$i]['user_black_list_id'] = $user_fr_main -> results()[$counterr] -> unique_id;
				$data["black_list-".$i]['user_black_list_name'] = $user_fr_main -> results()[$counterr] -> main_name;
				$data["black_list-".$i]['user_black_list_lastname'] = $user_fr_main -> results()[$counterr] -> main_lastname;
				$data["black_list-".$i]['user_black_list_avatar'] = $user_fr_main -> results()[$counterr] -> sys_avatar;
				$data["black_list-".$i]['user_black_list_status'] = $user_fr_main -> results()[$counterr] -> main_status;
				$data["black_list-".$i]['user_black_list_b_rank'] = $user_fr_main -> results()[$counterr] -> b_rank;
				$data["black_list-".$i]['user_black_list_b_rate'] = $user_fr_main -> results()[$counterr] -> b_rate;

				$shops = DB::getInstance() -> query("SELECT COUNT(*) AS shops FROM shops_relations WHERE user_id = '".$following->results()[$counterr-$black_list_count-$i-$i] -> user2."'");

				$fr = DB::getInstance() -> query("SELECT COUNT(*) AS friends FROM followers WHERE user1 = '".$following->results()[$counterr-$black_list_count-$i-$i] -> user2."'");
				
				$counterr++;
				$data["black_list-".$i]['user_black_list_shops'] = $shops -> results()[$counterr] -> shops;
				$counterr ++;


				$data["black_list-".$i]['user_black_list_fr'] = $fr -> results()[$counterr] -> friends;
				
				$counterr ++;
			}
		break;
		case 'wall':
			$user_wall = DB::getInstance() -> query("SELECT * FROM us_wall WHERE user_id = '".$params_value[0]."'");
			$user_wall_count = $user_wall -> count();
			$data["wall_count"] = $user_wall_count;
			$us_count = 1;
			$imgws_cout = $user_wall_count;
			$counterrr = $user_wall_count+$counterr-1;

			for($i = 0; $i < $user_wall_count; $i++)
			{
				if($user_wall -> results()[$counterr] -> from_id == $params_value[0])
				{
					$data[$i]["from_info"]["name"] = $users -> results()[0] -> main_name;
					$data[$i]["from_info"]["avatar"] = $users -> results()[0] -> sys_avatar;
				}
				else
				{
					$counterrr++;
					$from_info = DB::getInstance() -> query("SELECT * FROM us_main WHERE unique_id = '".$user_wall -> results()[$counterr] -> from_id."'");

					$data[$i]["from_info"]["name"] = $from_info -> results()[$counterrr] -> main_name;
					$data[$i]["from_info"]["avatar"] = $from_info -> results()[$counterrr] -> sys_avatar;
				}

				$data[$i]["wall_id"] = $user_wall -> results()[$counterr] -> id;
				$data[$i]["wall_descr"] = $user_wall -> results()[$counterr] -> descr;
				$data[$i]["wall_title"] = $user_wall -> results()[$counterr] -> title;
				$data[$i]["wall_type"] = $user_wall -> results()[$counterr] -> type;

				$now = time(); // текущее время (метка времени)
				$your_date = strtotime($user_wall -> results()[$counterr] -> date); // какая-то дата в строке (1 января 2017 года)
				$datediff = $now - $your_date; // получим разность дат (в секундах)

				$data[$i]["wall_date"] = floor($datediff / (60 * 60 * 24))." days ago";
				$data[$i]["wall_from_id"] = $user_wall -> results()[$counterr] -> from_id;
				$data[$i]["wall_back_set"] = "";

				$wall_settings = DB::getInstance() -> query("SELECT * FROM wall_settings WHERE wall_id = ".$user_wall -> results()[$counterr] -> id);
				
				$counterrr++;
				$data[$i]["wall_back_type"] = $wall_settings->results()[$counterrr] -> type;
				switch($wall_settings->results()[$counterrr] -> type)
				{
					case 'anim':
						$data[$i]["wall_back_set"] = $wall_settings->results()[$counterrr] -> gradient_anim;
					break;
					case 'grad':
						$data[$i]["wall_back_set"] = $wall_settings->results()[$counterrr] -> gradient_background;
					break;
					case 'col':
						$data[$i]["wall_back_set"] = $wall_settings->results()[$counterrr] -> color_background;
					break;
					case 'img':
						$data[$i]["wall_back_set"] = $wall_settings->results()[$counterrr] -> img_background;
					break;
				}
				/*$wall_imgs = DB::getInstance() -> query("SELECT * FROM wall_imgs WHERE wall_id = ".$user_wall -> results()[$counterr] -> id);

				$wall_imgs_count = $wall_imgs -> count();

				if($wall_imgs_count > 0)
				{
					for($j = 0; $j < $wall_imgs_count; $j++)
					{
						$counterrr++;
						$data[$i]["img-$j"]["name"] = $wall_imgs -> results()[$counterrr] -> name;
						$data[$i]["img-$j"]["id"] = $wall_imgs -> results()[$counterrr] -> id;
					}
				}
				$data[$i]["wall_imgs_count"] = $wall_imgs_count;*/

				$counterr++;
			}
			$followers_c_query = DB::getInstance() -> query("SELECT id FROM followers WHERE user2 = '".$params_value[0]."'");
			$data['my_info']["followers_c"] = $followers_c_query -> count();
			$following_c_query = DB::getInstance() -> query("SELECT id FROM followers WHERE user1 = '".$params_value[0]."'");
			$data['my_info']["following_c"] = $following_c_query -> count();
			$wall_c_query = DB::getInstance() -> query("SELECT id FROM us_wall WHERE user_id = '".$params_value[0]."'");
			$data['my_info']["wall_c"] = $wall_c_query -> count();
			$shops_c_query = DB::getInstance() -> query("SELECT id FROM us_shops WHERE admin_id = '".$params_value[0]."'");
			$data['my_info']["shops_c"] = $shops_c_query -> count();
		break;

	}


	$res = multi_thread_request(array("http://berna-diplom.norox.com.ua/api/get_notice/?".$params_name[0]."=".$params_name[0]));

	$data["dialog_notice"] = json_decode($res["http://berna-diplom.norox.com.ua/api/get_notice/?".$params_name[0]."=".$params_name[0]],true);
}
else
{
	$users = DB::getInstance() -> query('SELECT * FROM us_main WHERE unique_id = "'.$params_value[0].'"');

	$data = array(
								  'id' => $users -> results()[0] -> unique_id,
								  'sys_email' => $users -> results()[0] -> sys_email,
								  'sys_telephone' => $users -> results()[0] -> sys_telephone,
								  'sys_link' => $users -> results()[0] -> sys_link,
								  'sys_avatar' => $users -> results()[0] -> sys_avatar,
								  'main_name' => $users -> results()[0] -> main_name,
								  'main_lastname' => $users -> results()[0] -> main_lastname,
								  'sys_status' => $users -> results()[0] -> sys_status,
								  'sys_priv' => $users -> results()[0] -> sys_priv,
								  'b_rank' => $users -> results()[0] -> b_rank,
								  'b_rate' => $users -> results()[0] -> b_rate,
								  'main_status' => $users -> results()[0] -> main_status
								);
	$counterr = 1;

	switch($params_value[1])
	{
		case 'friends':
		$friends = explode(";",$users -> results()[0] -> rel_friends);

		$data['user_friends_count'] = count($friends);

		for($i = 0; $i < count($friends)-1; $i++)
		{
			$user_fr_main = DB::getInstance() -> query('SELECT unique_id,main_lastname,main_name,sys_avatar,main_status,b_rank,b_rate FROM us_main WHERE id='.$friends[$i]);

			$data[$i]['user_friends_id'] = $user_fr_main -> results()[$counterr] -> unique_id;
			$data[$i]['user_friends_name'] = $user_fr_main -> results()[$counterr] -> main_name;
			$data[$i]['user_friends_lastname'] = $user_fr_main -> results()[$counterr] -> main_lastname;
			$data[$i]['user_friends_avatar'] = $user_fr_main -> results()[$counterr] -> sys_avatar;
			$data[$i]['user_friends_status'] = $user_fr_main -> results()[$counterr] -> main_status;
			$data[$i]['user_friends_b_rank'] = $user_fr_main -> results()[$counterr] -> b_rank;
			$data[$i]['user_friends_shops'] = 3;
			$counterr ++;
		}
		break;
		case 'wall':
			$user_wall = DB::getInstance() -> query("SELECT * FROM us_wall WHERE user_id = '".$params_value[0]."'");
			$user_wall_count = $user_wall -> count();
			$data["wall_count"] = $user_wall_count;
			$us_count = 1;
			$imgws_cout = $user_wall_count;
			$counterrr = $user_wall_count+$counterr-1;

			for($i = 0; $i < $user_wall_count; $i++)
			{
				if($user_wall -> results()[$counterr] -> from_id == $params_value[0])
				{
					$data[$i]["from_info"]["name"] = $users -> results()[0] -> main_name;
					$data[$i]["from_info"]["avatar"] = $users -> results()[0] -> sys_avatar;
				}
				else
				{
					$counterrr++;
					$from_info = DB::getInstance() -> query("SELECT * FROM us_main WHERE unique_id = '".$user_wall -> results()[$counterr] -> from_id."'");

					$data[$i]["from_info"]["name"] = $from_info -> results()[$counterrr] -> main_name;
					$data[$i]["from_info"]["avatar"] = $from_info -> results()[$counterrr] -> sys_avatar;
				}

				$data[$i]["wall_id"] = $user_wall -> results()[$counterr] -> id;
				$data[$i]["wall_descr"] = $user_wall -> results()[$counterr] -> descr;
				$data[$i]["wall_title"] = $user_wall -> results()[$counterr] -> title;
				$data[$i]["wall_from_id"] = $user_wall -> results()[$counterr] -> from_id;

				$wall_imgs = DB::getInstance() -> query("SELECT * FROM wall_imgs WHERE wall_id = ".$user_wall -> results()[$counterr] -> id);

				$wall_imgs_count = $wall_imgs -> count();

				if($wall_imgs_count > 0)
				{
					for($j = 0; $j < $wall_imgs_count; $j++)
					{
						$counterrr++;
						$data[$i]["img-$j"]["name"] = $wall_imgs -> results()[$counterrr] -> name;
						$data[$i]["img-$j"]["id"] = $wall_imgs -> results()[$counterrr] -> id;
					}
				}
				$data[$i]["wall_imgs_count"] = $wall_imgs_count;

				$counterr++;
			}
			$followers_c_query = DB::getInstance() -> query("SELECT id FROM followers WHERE user2 = '".$params_value[0]."'");
			$data['my_info']["followers_c"] = $followers_c_query -> count();
			$following_c_query = DB::getInstance() -> query("SELECT id FROM followers WHERE user1 = '".$params_value[0]."'");
			$data['my_info']["following_c"] = $following_c_query -> count();
			$wall_c_query = DB::getInstance() -> query("SELECT id FROM us_wall WHERE user_id = '".$params_value[0]."'");
			$data['my_info']["wall_c"] = $wall_c_query -> count();
			$shops_c_query = DB::getInstance() -> query("SELECT id FROM us_shops WHERE admin_id = '".$params_value[0]."'");
			$data['my_info']["shops_c"] = $shops_c_query -> count();
		break;

	}


	$my_info = DB::getInstance() -> query('SELECT * FROM us_main WHERE unique_id = "'.$params_name[0].'"');

	$data['my_info'] = array(
	  'id' => $users -> results()[$counterr] -> unique_id,
	  'sys_email' => $users -> results()[$counterr] -> sys_email,
	  'sys_telephone' => $users -> results()[$counterr] -> sys_telephone,
	  'sys_link' => $users -> results()[$counterr] -> sys_link,
	  'sys_avatar' => $users -> results()[$counterr] -> sys_avatar,
	  'main_name' => $users -> results()[$counterr] -> main_name,
	  'main_lastname' => $users -> results()[$counterr] -> main_lastname,
	  'sys_status' => $users -> results()[$counterr] -> sys_status,
	  'sys_priv' => $users -> results()[$counterr] -> sys_priv,
	  'b_rank' => $users -> results()[$counterr] -> b_rank,
	  'main_status' => $users -> results()[$counterr] -> main_status
	);


	$res = multi_thread_request(array("http://berna-diplom.norox.com.ua/api/get_notice/?".$params_name[0]."=".$params_name[0]));

	$data["dialog_notice"] = json_decode($res["http://berna-diplom.norox.com.ua/api/get_notice/?".$params_name[0]."=".$params_name[0]],true);	
}
?>