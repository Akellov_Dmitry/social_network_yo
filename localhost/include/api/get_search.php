<?php
	if(!isset($params_value[1]) || $params_value[1] == "")
	{
		$users = DB::getInstance() -> query("SELECT `id`,`unique_id`,`main_lastname`,`main_name`,`main_status`,`sys_avatar`,`b_rank` FROM `us_main` WHERE `unique_id` <> ".$st." and `main_name` LIKE '%".htmlspecialchars(urldecode($params_value['0']))."%' OR `unique_id` <> ".$st." and `main_lastname` LIKE '%".htmlspecialchars(urldecode($params_value['0']))."%'");
		$us_c = $users -> count();
		$counter = $us_c-1;

		$html.="<input type='hidden' id='friends_search_count' value='".$us_c."'>";
		$html.="<ul>";
		for($i = 0; $i < $us_c; $i++)
		{
			$shops = DB::getInstance() -> query("SELECT COUNT(*) AS shops FROM shops_relations WHERE user_id = '".$users->results()[$i] -> unique_id."'");
			$counter++;
			$fr = DB::getInstance() -> query("SELECT COUNT(*) AS fr FROM followers WHERE user1 = '".$users->results()[$i] -> unique_id."'");
			$counter++;
			if($users->results()[$i] -> unique_id != $params_name[0])
			{
				$html.="<li>";
				$html.='<div>
					<i class="fas fa-user-slash" title="To black list"></i>';
				if($users->results()[$i] -> sys_avatar != "")
				{
					$html.="<a href='/my_page/user/".$users->results()[$i] -> unique_id."'><img src='../../img/".$users->results()[$i] -> sys_avatar."' /></a>";
				}
				else
				{
					$html.="<a href='/my_page/user/".$users->results()[$i] -> unique_id."'><img src='../../img/no_photo.jpg' /></a>";
				}
				$html.="<i class='fas fa-user-times' title='Delete'></i><a href='/my_page/user/".$users->results()[$i] -> unique_id."'>".$users->results()[$i] -> main_name." ".$users->results()[$i] -> main_lastname."</a>";
				$html.='<div>
									<div>
										<i class="far fa-star"></i>
										<label>'.$users->results()[$i] -> b_rank.'</label>
									</div>
									<div>
										<i class="fas fa-shopping-cart"></i>
										<label>'.$users->results()[$counter-1] -> shops.'</label>
									</div>
									<div>
										<i class="fas fa-users"></i>
										<label>'.$fr->results()[$counter] -> fr.'</label>
									</div>
								</div>';

				$is_fr_tr = false;
				$html.='</div>';
				$html.="<div>";
				if(!$is_fr_tr)
				{
					$html.="<i id='sow_add_fr_tr' class='fas fa-plus' title='Add to friends'></i>";
				}
				else
				{
					$html.="<i class='far fa-comment' title='Write message'></i>";
				}
				$html.='<i class="fa fa-phone" title="Audiocall to user"></i>';
				$html.='<i class="fa fa-video" title="Videocall to user"></i><input type="hidden" value="'.$users->results()[$i] -> unique_id.'"></div>';
				$html.="</li>";
			}
		}
		$html.="</ul>";
	}
	elseif($params_value[1] == "json")
	{
		$st = '"'.$_COOKIE["uid"].'"';
		$users = DB::getInstance() -> query("SELECT `id`,`unique_id`,`main_lastname`,`main_name`,`main_status`,`sys_avatar`,`b_rank` FROM `us_main` WHERE `unique_id` <> ".$st." and `main_name` LIKE '%".htmlspecialchars(urldecode($params_value['0']))."%' OR `unique_id` <> ".$st." and `main_lastname` LIKE '%".htmlspecialchars(urldecode($params_value['0']))."%'");

		$us_c = $users -> count();
		$counter = $us_c;
		$data["count"] = $counter;

		for($i = 0; $i < $us_c; $i++)
		{
			$shops = DB::getInstance() -> query("SELECT COUNT(*) AS shops FROM shops_relations WHERE user_id = '".$users->results()[$i] -> unique_id."'");
			$counter++;
			$fr = DB::getInstance() -> query("SELECT COUNT(*) AS fr FROM followers WHERE user1 = '".$users->results()[$i] -> unique_id."'");
			$counter++;

				$data["friend-$i"]["unique_id"] = $users->results()[$i] -> unique_id;
				$html.="<li>";
				$html.='<div>
					<i class="fas fa-user-slash" title="To black list"></i>';
				if($users->results()[$i] -> sys_avatar != "")
				{
					$html.="<a href='/my_page/user/".$users->results()[$i] -> unique_id."'><img src='../../img/".$users->results()[$i] -> sys_avatar."' /></a>";
					
					$data["friend-$i"]["sys_avatar"] = $users->results()[$i] -> sys_avatar;
				}
				else
				{
					$data["friend-$i"]["sys_avatar"] = "no_photo.jpg";
				}
				$data["friend-$i"]["main_name"] = $users->results()[$i] -> main_name;
				$data["friend-$i"]["main_lastname"] = $users->results()[$i] -> main_lastname;
				$data["friend-$i"]["b_rank"] = $users->results()[$i] -> b_rank;
				$data["friend-$i"]["shops"] = $users->results()[$counter-1] -> shops;
				$data["friend-$i"]["fr"] = $fr->results()[$counter] -> fr;

		}
	}
?>