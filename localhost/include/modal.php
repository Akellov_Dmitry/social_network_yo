<div id="create_dialog_modal">
	<div id="create_dialog_header">
		<p>Choose friend</p>
		<i class="fas fa-times" id="close_create_shop_modal"></i>
	</div>
	<div id="create_dialog_content">
		<ul>
		<?php for( $i = 0; $i < $data['user_friends_count']; $i++ ){ 
				$ava = ($data[$i]["user_friends_avatar"] == '') ? 'no_photo.jpg' : $data[$i]["user_friends_avatar"];
		?>
			<li>
				<img class="avatar_to_ch" src="/img/<?php echo $ava ?>" alt="<?php echo $ava ?>" />
				<p><?php echo $data[$i]["user_friends_name"].' '.$data[$i]["user_friends_lastname"]; ?></p>
			</li>
			<input type="hidden" value="<?php echo $data[$i]["user_friends_id"]; ?>" />
		<?php } ?>
		</ul>
	</div>
</div>

<div id="modal_create_wall">
	<div id="modal_create_wall_top">
		<h1>Create wall</h1>
		<i class="fas fa-times" id="close_create_wall_modal"></i>
	</div>
	<div>
		<div id="modal_create_wall_overflow">
			<svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
			   <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
			<p class="load_message">Your shop in creating. Wait until 3 minutes...</p>
		</div>
		<div>
			<i class="far fa-plus-square"></i>
			<img src="" alt="" id="modal_create_upl" />
			<input type="file" id="modal_create_upl" />
		</div>
		<div>
			<button id="create_wall_button">Add record</button></div>
	</div>
</div>

<div id="modal_add_wall">
	<div id="modal_add_wall_top">
		<h1>Add new proposal</h1>
		<div>
			<button id="active_wall_top_button">Proposal</button>
			<button>Thought</button>
			<input type="hidden" value="proposal" />
		</div>
		<button>Add</button>
		<i class="fas fa-times" id="close_create_shop_modal"></i>
	</div>
	<div>
		<div id="modal_add_wall_overflow">
			<svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
			   <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
			<p class="load_message">Your shop in creating. Wait until 3 minutes...</p>
		</div>
	</div>
	<div id="modal_add_wall_info">
		<label>Title:</label><input type="text" />
		<label>Description:</label><textarea name="" id="" rows="4"></textarea>
	</div>
	<div id="modal_add_wall_settings">
		<div id="modal_add_wall_settings_type">
			<button id="modal_add_wall_settings_type_active">Color</button>
			<button>Color transition</button>
			<button>Animation transition</button>
			<button>Image</button>
		</div>
		<div id="modal_add_wall_settings_color_bl" class="modal_add_wall_settings_bl modal_add_wall_settings_active">
			<label>Choose color of background:</label>
			<div id="modal_add_wall_settings_colorpicker"></div>
		</div>
		<div id="modal_add_wall_settings_colortr_bl" class="modal_add_wall_settings_bl">
			<label>Choose 2 colors of background:</label>
			<div id="modal_add_wall_settings_colorpicker"></div>
			<div id="modal_add_wall_settings_colorpicker"></div>
		</div>
		<div id="modal_add_wall_settings_anim_bl" class="modal_add_wall_settings_bl">
			<label>Choose 2 color of background:</label>
			<div id="modal_add_wall_settings_colorpicker"></div>
			<div id="modal_add_wall_settings_colorpicker"></div>
		</div>
		<div id="modal_add_wall_settings_image_bl" class="modal_add_wall_settings_bl">
			<label>Choose background image:</label>
			<input type="file" />
			<div id="modal_add_wall_settings_image_bl_prev">
				
			</div>
		</div>

	</div>
	<div id="modal_add_wall_cards">
		<p>Choose products for your promotion:</p>
		<div id="modal_add_wall_cards_shops">
			<?php for($i = $data["shops"]["count"]-1; $i >= 0; $i--){ ?>
				<div id="modal_add_wall_cards_shop">
					<div>
						<img src="<?php echo __NAME__; ?>img/shops_logo/<?php echo $data["shops"]["shop-$i"]["logo"]; ?>" alt="<?php echo $data["shops"]["shop-$i"]["logo"]; ?>">
					</div>
					<div>
						<p><?php echo $data["shops"]["shop-$i"]["name"]; ?></p>
						<p><?php echo $data["shops"]["shop-$i"]["descr"]; ?></p>
					</div>
					<input type="hidden" value="<?php echo $data["shops"]["shop-$i"]["id"]; ?>" />
				</div>
			<?php } ?>
		</div>
		<div id="modal_add_wall_cards_products">
			
		</div>
	</div>
	<div id="modal_add_wall_preview">
		<div id="modal_add_wall_preview_overlay"></div>
        <div id="modal_add_wall_preview_main" >
            
        </div>
        <div id="modal_add_wall_preview_cont">
                <p>Title</p>
                <p>Description</p>
            <div id="modal_add_wall_preview_cont_cards"></div>
        </div>
        <div id="modal_add_wall_preview_us_info">
            <?php if($data["wall_count"] > 0){ ?>
                <img src="../img/<?php echo $data[0]['from_info']['avatar']; ?>" />
                <p><?php echo $data[0]['from_info']['name']; ?></p>
            <?php } ?>
        </div>
        <input type="hidden" id="modal_add_wall_preview_cards_count" value="" />
        <input type="hidden" id="modal_add_wall_preview_back_type" value="col" />
        <input type="hidden" id="modal_add_wall_preview_back_color1" value="#424242" />
        <input type="hidden" id="modal_add_wall_preview_back_color2" value="" />
	</div>
</div>

<div id="modal_update_wall">
	<div id="modal_update_wall_top">
		<h1>Update proposal</h1>
		<div>
			<button id="active_wall_upd_top_button">Proposal</button>
			<button>Thought</button>
			<input type="hidden" value="proposal" />
		</div>
		<button>Update</button>
		<button>Delete</button>
		<i class="fas fa-times" id="close_create_shop_modal"></i>
	</div>
	<div>
		<div id="modal_update_wall_overflow">
			<svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
			   <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
			<p class="load_message">Your shop in creating. Wait until 3 minutes...</p>
		</div>
	</div>
	<div id="modal_update_wall_info">
		<label>Title:</label><input type="text" />
		<label>Description:</label><textarea name="" id="" rows="4"></textarea>
	</div>
	<div id="modal_update_wall_settings">
		<div id="modal_update_wall_settings_type">
			<button id="modal_update_wall_settings_type_active">Color</button>
			<button>Color transition</button>
			<button>Animation transition</button>
			<button>Image</button>
		</div>
		<div id="modal_update_wall_settings_color_bl" class="modal_update_wall_settings_bl modal_update_wall_settings_active">
			<label>Choose color of background:</label>
			<div id="modal_update_wall_settings_colorpicker"></div>
		</div>
		<div id="modal_update_wall_settings_colortr_bl" class="modal_update_wall_settings_bl">
			<label>Choose 2 colors of background:</label>
			<div id="modal_update_wall_settings_colorpicker"></div>
			<div id="modal_update_wall_settings_colorpicker"></div>
		</div>
		<div id="modal_update_wall_settings_anim_bl" class="modal_update_wall_settings_bl">
			<label>Choose 2 color of background:</label>
			<div id="modal_update_wall_settings_colorpicker"></div>
			<div id="modal_update_wall_settings_colorpicker"></div>
		</div>
		<div id="modal_update_wall_settings_image_bl" class="modal_update_wall_settings_bl">
			<label>Choose background image:</label>
			<input type="file" />
			<div id="modal_update_wall_settings_image_bl_prev">
				
			</div>
		</div>

	</div>
	<div id="modal_update_wall_cards">
		<p>Choose products for your promotion:</p>
		<div id="modal_update_wall_cards_shops">
			<?php for($i = $data["shops"]["count"]-1; $i >= 0; $i--){ ?>
				<div id="modal_update_wall_cards_shop">
					<div>
						<img src="<?php echo __NAME__; ?>img/shops_logo/<?php echo $data["shops"]["shop-$i"]["logo"]; ?>" alt="<?php echo $data["shops"]["shop-$i"]["logo"]; ?>">
					</div>
					<div>
						<p><?php echo $data["shops"]["shop-$i"]["name"]; ?></p>
						<p><?php echo $data["shops"]["shop-$i"]["descr"]; ?></p>
					</div>
					<input type="hidden" value="<?php echo $data["shops"]["shop-$i"]["id"]; ?>" />
				</div>
			<?php } ?>
		</div>
		<div id="modal_update_wall_cards_products">
			
		</div>
	</div>
	<div id="modal_update_wall_preview">
		<div id="modal_update_wall_preview_overlay"></div>
        <div id="modal_update_wall_preview_main" >
            
        </div>
        <div id="modal_update_wall_preview_cont">
                <p>Title</p>
                <p>Description</p>
            <div id="modal_update_wall_preview_cont_cards"></div>
        </div>
        <div id="modal_update_wall_preview_us_info">
            <?php if($data["wall_count"] > 0){ ?>
                <img src="../img/<?php echo $data[0]['from_info']['avatar']; ?>" />
                <p><?php echo $data[0]['from_info']['name']; ?></p>
            <?php } ?>
        </div>
        <input type="hidden" id="modal_update_wall_preview_cards_count" value="" />
        <input type="hidden" id="modal_update_wall_preview_back_type" value="col" />
        <input type="hidden" id="modal_update_wall_preview_back_color1" value="#424242" />
        <input type="hidden" id="modal_update_wall_preview_back_color2" value="" />
	</div>
	<input type="hidden" id="modal_update_wall_id" value="" />
</div>

<!--==========================Messages==========================-->
<div id="modal_new_message">
	<div id="modal_new_message_header">
		<div id="modal_new_message_header_img"><img src="./img/denia.png" alt="denia.png" /></div>
		<div><p id="modal_new_message_header_name">Denis Shatilovich</p></div>
		<div><i class="fas fa-times" id="close_create_shop_modal"></i></div>
	</div>
	<div id="modal_new_message_content">
		<textarea id="modal_new_message_content_text"></textarea>
		<div>
			<i class='far fa-smile'></i>
			<i class='fas fa-paperclip'></i>
			<i id='modal_new_message_content_new_mes_but' class='fas fa-chevron-circle-up'></i>
			<input id="modal_new_message_content_id" type='hidden' value='' />
		</div>
	</div>
</div>
<!--============================Shops modals==========================-->
<?php
	$url = explode('/', $_SERVER['REQUEST_URI']);
	if($url[1] == "shops" || $url[1] == "shop"){
?>
<div id="modal_cartung">
	<div id="modal_cartung_top">
		<label>Step 1/2</label>
		<i class="fas fa-times" id="close_create_shop_modal"></i>
	</div>
	<div id="modal_cartung_content">
		<div id="modal_cartung_overflow">
			<svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
			   <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
			<p class="load_message">Your shop in creating. Wait until 3 minutes...</p>
		</div>
		<div>
			<label>You buying <b><?php echo $data["cart"]["cartung"]["count"]; ?></b> products on sum <b><?php echo $data["cart"]["cartung"]["sum"]; ?></b> grn.</label>
			<label><i class="fas fa-trash-alt Clean"></i> Clean</label>
			<?php for($i = 0; $i < $data["cart"]["cartung"]["prod_count"]; $i++){ ?>
			<div class="modal_cartung_product">
				<div>
					<a href="<?php echo __NAME__ ?>shop/default/<?php echo $data["cart"]["prod-$i"]["shop_id"]; ?>"><img src="/img/products/<?php echo $data["cart"]["prod-$i"]["photo"]; ?>" alt="<?php echo $data["cart"]["prod-$i"]["photo"]; ?>"></a>
				</div>
				<div>
					<a href="<?php echo __NAME__ ?>shop/default/<?php echo $data["cart"]["prod-$i"]["shop_id"]; ?>"><?php echo $data["cart"]["prod-$i"]["name"]; ?></a>
					<p><?php echo $data["cart"]["prod-$i"]["cost"]; ?> grn.</p>
				</div>
				<div>
					<i class="fas fa-minus modal_cartung_product_minus"></i>
					<i class="fas fa-plus modal_cartung_product_plus"></i>
					<label><?php echo $data["cart"]["prod-$i"]["sum"]; ?> grn.</label>
					<label>for <b><?php echo $data["cart"]["prod-$i"]["count"]; ?></b> products</label>
					<input type="hidden" value="<?php echo $data["cart"]["prod-$i"]["id"]; ?>" />
					<input type="hidden" value="<?php echo $data["cart"]["prod-$i"]["cost"]; ?>" />
					<input type="hidden" value="<?php echo $data["cart"]["prod-$i"]["shop_id"]; ?>" />
				</div>
			</div>
			<?php } ?>
			<label id="create_order">Next step <i class="fas fa-long-arrow-alt-right"></i></label>
		</div>
	</div>
	<input type="hidden" id="cart_id" value="<?php echo $data["cart"]["cartung"]["id"]; ?>" />
</div>
<div id="modal_order">
	<div id="modal_order_top">
		<label>Step 2/2</label>
		<i class="fas fa-times" id="close_create_shop_modal"></i>
	</div>
	<div id="modal_order_content">
		<div>
			<!--<label>You buying <b><?php echo $data["cart"]["cartung"]["count"]; ?></b> products on sum <b><?php echo $data["cart"]["cartung"]["sum"]; ?></b> grn.</label>-->
			<div>
				<label>Add information for order:</label>
				<input type="text" placeholder="Name..." value="<?php echo $data['my_info']['main_name']; ?> <?php echo $data['my_info']['main_lastname']; ?>" />
				<input type="text" placeholder="City..." />
			</div>
			<div>
				<label>Choose type of payment:</label>
				<i class="fab fa-cc-visa" pay-type='visa'></i>
				<i class="fab fa-cc-mastercard" pay-type='mastercard'></i>
				<i class="fab fa-paypal" pay-type='paypal'></i>
				<i class="fab fa-cc-apple-pay" pay-type='applepay'></i>
			</div>
			<label id="back_to_cart"><i class="fas fa-long-arrow-alt-left"></i> Back</label>
			<label id="do_payment"><i class="fas fa-money-check-alt"></i> Pay</label>
			<input type="hidden" value='' />
		</div>
	</div>
	<input type="hidden" id="cart_id" value="<?php echo $data["cart"]["cartung"]["id"]; ?>" />
</div>
<?php } ?>


<div id="modal_create_shop">
	<div id="modal_create_shop_top">
		<h1>Create new shop</h1>
		<i class="fas fa-times" id="close_create_shop_modal"></i>
	</div>
	<div>
		<div id="modal_create_shop_overflow">
			<svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
			   <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
			<p class="load_message">Your shop in creating. Wait until 3 minutes...</p>
		</div>
		<div>
			<i class="far fa-plus-square"></i>
			<img src="" alt="" id="modal_create_upl" />
			<input type="file" id="modal_create_upl" />
		</div>
		<div>
			<p></p>
			<input type="text" placeholder="Name..." />
			<textarea placeholder="Description(to 450 symbols)..."></textarea>
			<button id="create_shop_button">Create</button>
		</div>
	</div>
</div>

<div id="modal_update_shop">
	<div id="modal_update_shop_top">
		<h1>Update shop</h1>
		<i class="fas fa-times" id="close_update_shop_modal"></i>
	</div>
	<div>
		<div id="modal_update_shop_overflow">
			<svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
			   <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
			<p class="load_message">Your shop in updating. Wait until 3 minutes...</p>
		</div>
		<div>
			<i class="far fa-plus-square"></i>
			<img src="" alt="" id="modal_update_upl" />
			<input type="file" id="modal_update_upl" />
		</div>
		<div>
			<p></p>
			<input type="text" placeholder="Name..." />
			<textarea placeholder="Description(to 450 symbols)..."></textarea>
			<button id="update_shop_button">Update</button>
		</div>
		<input type="hidden" id="modal_update_shop_id" value="" />
	</div>
</div>

<div id="modal_add_prod">
	<div id="modal_add_prod_top">
		<h1>Add product</h1>
		<i class="fas fa-times" id="close_create_shop_modal"></i>
	</div>
	<div>
		<div id="modal_add_prod_overflow">
			<svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
			   <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
			<p class="load_message">Product added to shop <?php echo $data["shop"]["shop_name"]; ?>...</p>
		</div>
		<div>
			<i class="far fa-plus-square"></i>
			<img src="" alt="" id="modal_create_upl" />
			<input type="file" id="modal_create_upl" />
		</div>
		<div>
			<p></p>
			<input type="text" placeholder="Name..." />
			<input type="text" placeholder="Cost..." />
			<textarea placeholder="Description(to 450 symbols)..."></textarea>
			<button id="create_prod_button">Add</button>
			<input type="hidden" value='' />
		</div>
	</div>
</div>

<div id="modal_update_prod">
	<div id="modal_update_prod_top">
		<h1>Updating product</h1>
		<div><i class="fas fa-trash-alt"></i> Delete</div>
		<input type="hidden" value="" />
		<i class="fas fa-times" id="close_create_shop_modal"></i>
	</div>
	<div>
		<div id="modal_update_prod_overflow">
			<svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
			   <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
			<p class="load_message">Product updated to shop <?php echo $data["shop"]["shop_name"]; ?>...</p>
		</div>
		<div>
			<i class="far fa-plus-square"></i>
			<img src="" alt="" id="modal_create_upl" />
			<input type="file" id="modal_create_upl" />
		</div>
		<div>
			<p></p>
			<input type="text" placeholder="Name..." />
			<input type="text" placeholder="Cost..." />
			<textarea placeholder="Description(to 450 symbols)..."></textarea>
			<button id="update_prod_button">Update</button>
			<input type="hidden" value='<?php echo $data["shop"]["shop_id"]; ?>' />
		</div>
	</div>
</div>

<div id="page_add_avatar_modal">
    <button>X</button>
<?php $session_id=$_COOKIE['uid']; ?>
		<p>
			<label for="image">Image:</label>
			<input type="file" name="image" id="add_avatar_new_image" />
		</p>
		<p>
			<input type="button" id="submit_new_avatar" value="Upload!" />
		</p>
		<div id="page_add_avatar_preview"></div>
</div>

<script src="https://www.gstatic.com/firebasejs/3.6.4/firebase.js"></script>

<!--<div id="video_call_modal" class="modal_window">
	<div id="aded_modal_header">
		<div>
		</div>
		<div><a class="modal_window_close modal_cideo_call_modal_close">Х</a></div>
	</div>
	<div id="video_call_modal_content">
		 <video id="yourVideo" autoplay muted></video>
		 <video id="friendsVideo" autoplay></video>
		 <br />
		 <button onclick="showFriendsFace()" type="button" class="call_button"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <i class="fas fa-phone-volume"></i></button>
		 <button onclick="hangUpCall()" type="button" class="end_call_button"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <i class="fas fa-phone"></i></button>
		 <button onclick="muteVideo()">Откл. камеру</button>
		 <button onclick="muteAudio()">Откл. звук</button>
	</div>
</div>

<div id="video_call_modal" class="modal_window">
	<div id="aded_modal_header">
		<div>
		</div>
		<div><a class="modal_window_close modal_cideo_call_modal_close">Х</a></div>
	</div>
	<div id="video_call_modal_content">
		 <video id="yourVideo" autoplay muted></video>
		 <video id="friendsVideo" autoplay></video>
		 <br />
		 <button onclick="invite()" type="button" class="call_button"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <i class="fas fa-phone-volume"></i></button>
		 <button onclick="hangUpCall()" type="button" class="end_call_button"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <i class="fas fa-phone"></i></button>
		 <button>Откл. камеру</button>
		 <button>Откл. звук</button>
	</div>
</div>-->