-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 21, 2018 at 08:30 PM
-- Server version: 10.1.37-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yofun_yo`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartung`
--

CREATE TABLE `cartung` (
  `id` int(11) NOT NULL,
  `cost` varchar(8) NOT NULL,
  `count` int(11) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cartung`
--

INSERT INTO `cartung` (`id`, `cost`, `count`, `user_id`, `order_id`) VALUES
(11, '7580', 5, '5bcd35c34e33a6.41932717', 11);

-- --------------------------------------------------------

--
-- Table structure for table `cart_prod`
--

CREATE TABLE `cart_prod` (
  `id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `sum` varchar(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_prod`
--

INSERT INTO `cart_prod` (`id`, `prod_id`, `shop_id`, `cart_id`, `count`, `sum`) VALUES
(11, 3, 11, 11, 3, '3600'),
(12, 2, 3, 11, 2, '3980');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(11) NOT NULL,
  `user1` varchar(23) NOT NULL,
  `user2` varchar(23) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user1`, `user2`) VALUES
(9, '5bcd35c34e33a6.41932717', '5bcf42f6a753d3.88619982'),
(10, '5bcf42f6a753d3.88619982', '5bcd35c34e33a6.41932717'),
(14, '5bcd35c34e33a6.41932717', '5bce0816473da3.05896179'),
(13, '5bce0816473da3.05896179', '5bcf42f6a753d3.88619982'),
(15, '5bcd35c34e33a6.41932717', '5bce0a163c1550.93310312'),
(16, '5bce0816473da3.05896179', '5bcd35c34e33a6.41932717');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `sum` varchar(8) NOT NULL,
  `from_id` varchar(30) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `sum`, `from_id`, `date`) VALUES
(11, '7580', '5bcd35c34e33a6.41932717', '2018-12-15 02:36:40');

-- --------------------------------------------------------

--
-- Table structure for table `sh_product`
--

CREATE TABLE `sh_product` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `descr` text NOT NULL,
  `cost` varchar(8) NOT NULL,
  `photo` varchar(250) NOT NULL,
  `shop_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sh_product`
--

INSERT INTO `sh_product` (`id`, `name`, `descr`, `cost`, `photo`, `shop_id`) VALUES
(2, 'Product', 'Description', '1990', '1.png', 3),
(3, 'Akellow', 'dsf', '1200', '11-akellov.jpg', 11);

-- --------------------------------------------------------

--
-- Table structure for table `us_comments`
--

CREATE TABLE `us_comments` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `text` int(11) NOT NULL,
  `type` enum('wall','avatar','shop','product') NOT NULL,
  `from_id` varchar(23) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `us_dialogs`
--

CREATE TABLE `us_dialogs` (
  `id` int(11) NOT NULL,
  `users` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `us_dialogs`
--

INSERT INTO `us_dialogs` (`id`, `users`) VALUES
(44, '5bce0816473da3.05896179;5bcd35c34e33a6.41932717'),
(45, '5bcf42f6a753d3.88619982;5bcd35c34e33a6.41932717'),
(46, '5bce0816473da3.05896179;5bce0a163c1550.93310312'),
(47, '5bce0816473da3.05896179;5bcf42f6a753d3.88619982');

-- --------------------------------------------------------

--
-- Table structure for table `us_main`
--

CREATE TABLE `us_main` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(23) NOT NULL,
  `sys_email` varchar(100) NOT NULL,
  `sys_telephone` varchar(30) NOT NULL,
  `encrypted_password` varchar(250) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `sys_created_at` datetime NOT NULL,
  `sys_updated_as` datetime DEFAULT NULL,
  `sys_link` varchar(25) DEFAULT NULL,
  `sys_avatar` varchar(200) DEFAULT NULL,
  `sys_status` enum('active','banned') NOT NULL,
  `sys_priv` enum('user','admin') NOT NULL,
  `main_name` varchar(30) NOT NULL,
  `main_lastname` varchar(150) NOT NULL,
  `main_status` varchar(250) DEFAULT NULL,
  `rel_shops` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `us_main`
--

INSERT INTO `us_main` (`id`, `unique_id`, `sys_email`, `sys_telephone`, `encrypted_password`, `salt`, `sys_created_at`, `sys_updated_as`, `sys_link`, `sys_avatar`, `sys_status`, `sys_priv`, `main_name`, `main_lastname`, `main_status`, `rel_shops`) VALUES
(75, '5bcf42f6a753d3.88619982', 'sara.ray@hq.com', '', 'zbtsXmaiZyNQQ1+RwVQZ+nfnFhg4ZDZlMzFlYjc5', '8d6e31eb79', '0000-00-00 00:00:00', NULL, '', 'no_photo.jpg', 'active', 'user', 'Sara', 'Ray', '', ''),
(74, '5bce0a163c1550.93310312', 'noverkoanton@protonmail.com', '', 'la0g+Kd5w0cprLG+EYV4ERPG/T4wOGRjZGJjNWFl', '08dcdbc5ae', '0000-00-00 00:00:00', NULL, '', '', 'active', 'user', 'Tony', '', '', ''),
(57, '5bcd35c34e33a6.41932717', 'bxack911@gmail.com', '', 'FlpS7ZALMjbamMcxR5GzS9jZHLAzMmIwYTA4OTBm', '32b0a0890f', '0000-00-00 00:00:00', NULL, '', '', 'active', 'user', 'Max', 'berna', 'I\'m berna', '11;'),
(73, '5bce0816473da3.05896179', 'yo.support@gmail.com', '380978754690', 'eHEpyUc/U6WKbI8Qf1BSjAvY7NM0YTZiMjBlNjJm', '4a6b20e62f', '0000-00-00 00:00:00', NULL, 'akellov', 'akellov.jpg', 'active', 'user', 'Dmitry', 'Akellov', 'Y_O.fun SOcial Network', '');

-- --------------------------------------------------------

--
-- Table structure for table `us_messages`
--

CREATE TABLE `us_messages` (
  `id` int(11) NOT NULL,
  `from_id` varchar(30) NOT NULL,
  `dialog_id` int(11) NOT NULL,
  `text` mediumtext NOT NULL,
  `date` date NOT NULL,
  `is_read` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `us_messages`
--

INSERT INTO `us_messages` (`id`, `from_id`, `dialog_id`, `text`, `date`, `is_read`) VALUES
(151, '5bcd35c34e33a6.41932717', 43, 'SGkuIEkgd2FudCB0byBzdGFydCBjb21tdW5pdHkgd2l0aCB5b3Uh', '2018-10-23', 0),
(167, '5bcd35c34e33a6.41932717', 43, 'dA==', '2018-10-23', 0),
(168, '5bcd35c34e33a6.41932717', 43, 'amxrag==', '2018-10-23', 0),
(169, '5bcd35c34e33a6.41932717', 43, 'amxrag==', '2018-10-23', 0),
(170, '5bcd35c34e33a6.41932717', 43, 'amxrag==', '2018-10-23', 0),
(171, '5bcd35c34e33a6.41932717', 43, 'amxrag==', '2018-10-23', 0),
(172, '5bcd35c34e33a6.41932717', 43, '', '2018-10-23', 0),
(173, '5bcd35c34e33a6.41932717', 43, '', '2018-10-23', 0),
(174, '5bce0816473da3.05896179', 44, 'SGkuIEkgd2FudCB0byBzdGFydCBjb21tdW5pdHkgd2l0aCB5b3Uh', '2018-10-24', 0),
(175, '5bce0816473da3.05896179', 44, 'RnVjaw==', '2018-10-24', 0),
(176, '5bce0816473da3.05896179', 44, 'RnVjaw==', '2018-10-24', 0),
(177, '5bce0816473da3.05896179', 44, 'RnVjaw==', '2018-10-24', 0),
(178, '5bcd35c34e33a6.41932717', 43, '', '2018-10-24', 0),
(179, '5bce0816473da3.05896179', 44, 'c3lrYQ==', '2018-10-24', 0),
(180, '5bcf42f6a753d3.88619982', 45, 'SGkuIEkgd2FudCB0byBzdGFydCBjb21tdW5pdHkgd2l0aCB5b3Uh', '2018-10-24', 0),
(181, '5bcd35c34e33a6.41932717', 45, '0L/RgNC40LLQtdGC', '2018-10-24', 0),
(182, '5bcd35c34e33a6.41932717', 44, 'Z2hiZHRu', '2018-10-24', 0),
(183, '5bcd35c34e33a6.41932717', 45, '0L7Qu9C+0Ls=', '2018-10-25', 0),
(184, '5bcd35c34e33a6.41932717', 45, '0J/RgNC40LLQtdGCINCx0YDQvg==', '2018-10-28', 0),
(185, '5bcd35c34e33a6.41932717', 43, 'ZHNmZHNmZA==', '2018-10-28', 0),
(186, '5bcd35c34e33a6.41932717', 43, 'ZHNmc2Rmc2Q=', '2018-10-28', 0),
(187, '5bcd35c34e33a6.41932717', 44, 'ZXdmd2Vmd2U=', '2018-10-28', 0),
(188, '5bcd35c34e33a6.41932717', 43, 'ZXdmd2Vmd2U=', '2018-10-28', 0),
(189, '5bcd35c34e33a6.41932717', 43, 'ZXdmd2Vmd2U=', '2018-10-28', 0),
(190, '5bcd35c34e33a6.41932717', 43, 'ZXdmd2Vmd2U=', '2018-10-28', 0),
(191, '5bcd35c34e33a6.41932717', 43, 'ZXdmd2Vmd2U=', '2018-10-28', 0),
(192, '5bcd35c34e33a6.41932717', 43, 'ZXdmd2Vmd2U=', '2018-10-28', 0),
(193, '5bcd35c34e33a6.41932717', 43, 'ZXdmd2Vmd2U=', '2018-10-28', 0),
(194, '5bcd35c34e33a6.41932717', 43, 'ZXdmd2Vmd2U=', '2018-10-28', 0),
(195, '5bcd35c34e33a6.41932717', 43, 'ZmRnZGZnZGY=', '2018-10-28', 0),
(196, '5bcd35c34e33a6.41932717', 43, 'ZmRnZGZnZGY=', '2018-10-28', 0),
(197, '5bcd35c34e33a6.41932717', 43, 'ZmRnZGZnZGY=', '2018-10-28', 0),
(198, '5bcd35c34e33a6.41932717', 43, 'ZmRnZGZnZGY=', '2018-10-28', 0),
(199, '5bcd35c34e33a6.41932717', 43, 'ZmRnZGZnZGY=', '2018-10-28', 0),
(200, '5bcd35c34e33a6.41932717', 43, 'ZmRnZGZnZGY=', '2018-10-28', 0),
(201, '5bcd35c34e33a6.41932717', 44, 'ZmRnZGY=', '2018-10-28', 0),
(202, '5bcd35c34e33a6.41932717', 44, 'ZmRnZGY=', '2018-10-28', 0),
(203, '5bcd35c34e33a6.41932717', 44, 'ZmRnZGY=', '2018-10-28', 0),
(204, '5bce0816473da3.05896179', 44, 'Yml0Y2g=', '2018-10-29', 0),
(205, '5bce0816473da3.05896179', 44, 'Yml0Y2g=', '2018-10-29', 0),
(206, '5bce0816473da3.05896179', 44, 'Yml0Y2g=', '2018-10-29', 0),
(207, '5bce0816473da3.05896179', 44, 'Yml0Y2g=', '2018-10-29', 0),
(208, '5bce0816473da3.05896179', 44, 'Yml0Y2g=', '2018-10-29', 0),
(209, '5bce0816473da3.05896179', 44, 'Yml0Y2g=', '2018-10-29', 0),
(210, '5bce0816473da3.05896179', 44, 'Yml0Y2g=', '2018-10-29', 0),
(211, '5bce0816473da3.05896179', 44, 'Yml0Y2g=', '2018-10-29', 0),
(212, '5bce0816473da3.05896179', 44, 'Yml0Y2g=', '2018-10-29', 0),
(213, '5bcd35c34e33a6.41932717', 45, 'cmVmcmVncmU=', '2018-11-02', 0),
(214, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(215, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(216, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(217, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(218, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(219, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(220, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(221, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(222, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(223, '5bcd35c34e33a6.41932717', 45, 'aGtqaGtq', '2018-11-02', 0),
(224, '5bcd35c34e33a6.41932717', 44, 'ZXdmd2U=', '2018-11-02', 0),
(225, '5bce0816473da3.05896179', 44, 'ZnNkaA==', '2018-11-06', 0),
(226, '5bcd35c34e33a6.41932717', 44, 'aGpraGtq', '2018-11-09', 0),
(227, '5bcd35c34e33a6.41932717', 44, 'aGpraGtq', '2018-11-09', 0),
(228, '5bce0816473da3.05896179', 44, 'dXlnc2FqXA==', '2018-11-13', 0),
(229, '5bce0816473da3.05896179', 44, 'cnR1ZQ==', '2018-11-13', 0),
(230, '5bce0816473da3.05896179', 44, 'cnR1ZWhzZGthbA==', '2018-11-13', 0),
(231, '5bce0816473da3.05896179', 46, 'SGkuIEkgd2FudCB0byBzdGFydCBjb21tdW5pdHkgd2l0aCB5b3Uh', '2018-11-13', 0),
(232, '5bce0816473da3.05896179', 47, 'SGkuIEkgd2FudCB0byBzdGFydCBjb21tdW5pdHkgd2l0aCB5b3Uh', '2018-11-13', 0),
(233, '5bcd35c34e33a6.41932717', 44, 'Z2c=', '2018-11-20', 0),
(234, '5bce0816473da3.05896179', 44, '0L/Qt9C00YY=', '2018-11-20', 0),
(235, '5bce0816473da3.05896179', 44, 'dHl0', '2018-11-20', 0),
(236, '', 44, '', '2018-11-20', 0),
(237, '5bce0816473da3.05896179', 44, '', '2018-11-20', 0);

-- --------------------------------------------------------

--
-- Table structure for table `us_shops`
--

CREATE TABLE `us_shops` (
  `id` int(11) NOT NULL,
  `admin_id` varchar(30) NOT NULL,
  `name` varchar(150) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `descr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `us_shops`
--

INSERT INTO `us_shops` (`id`, `admin_id`, `name`, `logo`, `descr`) VALUES
(3, '10', 'Magaz', '3-3-master.jpg', 'Description'),
(11, '5bcd35c34e33a6.41932717', 'sad', '5bcd35c34e33a6.41932717-3-modnica.jpg', 'fds');

-- --------------------------------------------------------

--
-- Table structure for table `us_wall`
--

CREATE TABLE `us_wall` (
  `id` int(11) NOT NULL,
  `user_id` varchar(23) NOT NULL,
  `title` varchar(20) NOT NULL,
  `descr` text NOT NULL,
  `from_id` varchar(23) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `us_wall`
--

INSERT INTO `us_wall` (`id`, `user_id`, `title`, `descr`, `from_id`) VALUES
(1, '5bcd35c34e33a6.41932717', 'title', 'descr', '5bce0816473da3.05896179'),
(6, '5bcd35c34e33a6.41932717', 'title1', 'descr1', '5bcd35c34e33a6.41932717');

-- --------------------------------------------------------

--
-- Table structure for table `wall_imgs`
--

CREATE TABLE `wall_imgs` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `wall_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wall_imgs`
--

INSERT INTO `wall_imgs` (`id`, `name`, `wall_id`) VALUES
(1, 'headers.jpg', 1),
(6, '3-modnica.jpg', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartung`
--
ALTER TABLE `cartung`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_prod`
--
ALTER TABLE `cart_prod`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sh_product`
--
ALTER TABLE `sh_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `us_comments`
--
ALTER TABLE `us_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `us_dialogs`
--
ALTER TABLE `us_dialogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `us_main`
--
ALTER TABLE `us_main`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`unique_id`),
  ADD UNIQUE KEY `sys_email` (`sys_email`);

--
-- Indexes for table `us_messages`
--
ALTER TABLE `us_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `us_shops`
--
ALTER TABLE `us_shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `us_wall`
--
ALTER TABLE `us_wall`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wall_imgs`
--
ALTER TABLE `wall_imgs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cartung`
--
ALTER TABLE `cartung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cart_prod`
--
ALTER TABLE `cart_prod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sh_product`
--
ALTER TABLE `sh_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `us_comments`
--
ALTER TABLE `us_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `us_dialogs`
--
ALTER TABLE `us_dialogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `us_main`
--
ALTER TABLE `us_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `us_messages`
--
ALTER TABLE `us_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=238;

--
-- AUTO_INCREMENT for table `us_shops`
--
ALTER TABLE `us_shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `us_wall`
--
ALTER TABLE `us_wall`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wall_imgs`
--
ALTER TABLE `wall_imgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
